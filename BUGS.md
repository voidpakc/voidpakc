# Known bugs:

## General:

### Tyr-Quake control problems

TODO: Report upstream!

 - After enabling the mouse, it can't be bound to anything new. As a
   workaround, turn off "Use mouse", restart, reconfigure controls
   then enable "Use mouse" again.
 - There are a couple of problems with using ctrl for attack. If you
   don't have a mouse I recommend enabling always run and using lshift
   for attack instead.
