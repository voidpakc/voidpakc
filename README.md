# voidpakc

## WARNING:

**This is currently alpha software. It has not been tested much, and
things may completely change without notice.**

<hr>

<br>

voidpakc is a package management tool that uses some of the
technologies also used by AppImage, Flatpak and Snap, but in a format
closer to SlackBuilds, BSD ports trees and the Void Linux packaging
system.

As you may have guessed, this is based on Void Linux, but you don't
have to have it installed to use this.

Everything builds and runs in containers to provide a consistent
environment on all platforms, to allow installation without using
root, and to prevent programs from eating all of your files.

All the build scripts and metadata files are in this repo. Right now,
they are all games, but GTK is way too bloated and I don't see any
point in containerising `cut(1)`. Maybe Qt would be a little better,
except that the Void package has gtk3 as a dependency...

<hr>

This is currently source-only, but we use a lot of binary packages
from Void so it isn't too bad - compilation of the base image should
be less than half an hour on just about any system.

Both the runtime container images and the game images are in squashfs
archives, which allows for fast access and reasonable compression, and
has in-kernel support, though squashfuse is used to avoid having to
use root.

You are encouraged to look through the scripts before
running them. Bonus points if you can cross off one of the TODOs
and send an MR.

## Installation:

### Get the source

```
$ git clone https://gitlab.com/voidpakc/voidpakc
```

If you don't have git, you can try something like this instead:

```
$ wget -O- https://gitlab.com/voidpakc/voidpakc/archive/master.tar.bz2 | tar -xvJ
```

### Configuration

Having a read through the config is a good idea, but just leaving the
defaults won't blow anything up.

```
$ cat config-default.ini
$ EDITOR config.ini
```

### Create build container

This creates a new minimal Void Linux build container, and fills in
where it put it to Paths / bootstrap in the config file.

```
$ ./bootstrap-void.sh
```

### Create runtime images

This downloads a bunch of packages, compiles a few things then creates
two squashfs images, the runtime, and devel runtime.

The runtime, which is just over 30M for armv7l or 40M for amd64,
contains common libraries such as SDL, as well as coreutils and other
common GNU utilities.

The devel runtime is larger at around 160M on ARM and 210M on x86, but
it would be closer to 610M uncompressed. In terms of size, the biggest
offenders here are GCC, Python, Perl, git, CMake and glibc.

The devel runtime also includes gdb, and both runtimes have strace.

This script is designed so that you can run it multiple times on the
same container, but there is no guarantee that will produce working
runtime images, especially if it has been a while since you created
the container.

```
$ ./install-images.sh
```

You will now have 1.5G-2G of files in `$VOID_DIR`, depending on
architecture. You can safely remove the files with something like:

```
$ rm -rf --one-file-system /tmp/voidtmp
```

### Compile some games

```
$ ./compile.py chromium-bsu
```

You can change where build files are put (the default is `/tmp`) with
`TMPDIR=/path/to/dir/ ./comp...`.

Build scripts are located in `scripts/`. If you are the sort of Zsh
user who swaps the key bindings for space and tab, you can take
advantage of tab completion by doing:

```
$ ./compile.py s<TAB><TAB><TAB>
```

You might want to look at the `info` file in each directory, which has
some information about the game, including the sizes for the source,
build dir and binary image when I last compiled it. I might add a
`desc` file some day.

### Play some games

```
$ ./runner.py chromium-bsu
```

Games should run at near-native speed, but loading may be slightly
slower for the first launch after reboot.

The runner script only uses files in `$IMAGE_DIR`. Take a `find(1)`
around there if you want.

Note that this script launches the game in an empty environment, so if
you want to set any environment variables, you'll have to do it like this:

```
$ ./runner.py LIBGL_ALWAYS_SOFTWARE=1 chromium-bsu
```

You can also pass command-line arguments to the game the usual way.

If you want to share configuration and save files etc. with system
installations, you can do something like (replacing `$IMAGE_DIR` with
the value you set in the config file):

```
$ eval $(python3 ./lib/config.py --env --get Paths images)
$ mv $IMAGES/conf/lzdoom{,.old}
$ ln -s ~/.config/lzdoom $IMAGES/conf/lzdoom
```

### Data dependencies

Sometimes, the binary package isn't enough to run a game. \
For example, LZDoom only ships binaries and some support files,
and doesn't include any actual game data files.

If you try running lzdoom without installing and DOOM data packages,
it will complain, and suggest some packages to install, which in this
case includes `nonfree/doom-sw` (shareware DOOM) and `freedoom`
(BSD-licensed DOOM compatible wads).

If you have more than one data package for a game installed, you will
be prompted for which one you want to use on start.

## Q. Why Void Linux?

It has good architecture support, a good amount of binary packages, a
decent package build system, the package manager isn't impossible to
work with, it isn't dead and it ships static libraries.

I would have chosen Slackware but the install script would have to
build far more packages from source.

## Q. You spelt "pack" wrong

At least all the right letters are there...
