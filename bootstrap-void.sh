#!/bin/bash

REL=xbps-static-static-0.56_5
WGETOPT=( "--progress=dot:mega" )

usage() {
    printf "USAGE: %s [XBPS-STATIC.TAR.XZ]\n" "$0"
}

if ! [ -e "$(basename "$0")" ] && ! [ -e bootstrap-void.sh ]; then
    printf "This script must be run in the repo directory\n"
    exit 1
fi

if [ "${1:0:1}" = "-" ]; then
    usage
    exit 1
fi

eval $(python3 ./lib/config.py --env \
               --get "Container" "socks proxy" \
               --get "Paths" "base image build" \
               --get "Void" "mirror" \
               --get "Void" "package cache" \
    )

if [ "$?" != 0 ]; then
    printf "The config system is broken\n"
    printf "Maybe you don't have a new enough python3\n"
    exit 1
fi


set -e


VOID_DIR="$BASE_IMAGE_BUILD"

printf "Creating installation directory %s...\n" "$VOID_DIR"

if ! mkdir -p "$VOID_DIR"; then
    usage
    printf "Failed to create installation directory %s\n" "$VOID_DIR"
    exit 1
fi

INSTALLDIR="$(mktemp -d "$(date -Iminutes).XXXXXX" -p "$(realpath "$VOID_DIR")")"

if ! [ -d "$INSTALLDIR" ]; then
    printf "Failed to create installation subdirectory\n"
    exit 1
fi

SIZEAVAIL="$(df -kP "$INSTALLDIR" | tail -n1 | awk '{ print $4; }')"

if [ "$SIZEAVAIL" -lt 2621440 ]; then
    printf "WARNING: Less than 2.5 GB storage available on %s.\n" "$(df -kP "$INSTALLDIR" | tail -n1 | awk '{ print $6; }')"
    printf "See the config file for recommendations.\n"
fi

python3 ./lib/config.py --set Void bootstrap "$INSTALLDIR"

if ! [ "$ARCH" ]; then
    case "$(uname -m)" in
        arm | armv6*)
            ARCH=armv6l
            ;;
        armv7*)
            ARCH=armv7l
            ;;
        aarch64)
            ARCH=aarch64
            ;;
        *64)
            ARCH=x86_64
            ;;
        *86)
            ARCH=i686
            REL=xbps-static-static-0.51_1
            ;;
        *)
            printf "Unknown arch %s\n" "$(uname -m)"
            exit 1
            ;;
    esac
fi

if [ "$1" ]; then
    XBPSPATH="$(realpath "$1")"
fi

cd "$INSTALLDIR"

mkdir static
cd static
if ! [ "$XBPSPATH" ]; then
    DL="$MIRROR/static/$REL.$ARCH-musl.tar.xz"
    XBPSPATH=xbps.txz
    echo Downloading "$DL"...
    if hash curl 2>/dev/null; then
        if [ "$SOCKS_PROXY" ]; then
            printf "Using proxy %s\n" "$SOCKS_PROXY"
            # sed socks5 -> socks5h to proxy DNS
            curl -# -x "$(printf "%s" "$SOCKS_PROXY" | sed 's/socks5:/socks5h:/')" "$DL" -o "$XBPSPATH"
        else
            curl -# "$DL" -o "$XBPSPATH"
        fi
    elif hash wget 2>/dev/null; then
        if [ "$SOCKS_PROXY" ]; then
            printf "wget does not support SOCKS proxies. Download\n%s\nmanually" "$DL"
            printf " or install curl\n"
            exit 1
        fi
        wget "${WGETOPT[@]}" "$DL" -O "$XBPSPATH"
    else
        printf "Either wget or curl must be installed to download xbps\n"
        printf "Alternatively, download %s\nmanually and give it as an argument.\n" "$DL"
    fi
fi
tar xf "$XBPSPATH"

if [ "$PACKAGE_CACHE" ]; then
    CACHE_OPTS=( -c "$PACKAGE_CACHE" )
fi

REPO="$MIRROR"/current

# TODO: Test aarch64 with musl
if [ "$ARCH" = aarch64 ]; then
    REPO="$REPO"/aarch64
fi

if [ "$MUSL" = 1 ]; then
    export XBPS_ARCH="$ARCH-musl"
    REPO="$REPO"/musl
fi

export SOCKS_PROXY

yes | ./usr/bin/xbps-install -R "$REPO" -r "$INSTALLDIR" "${CACHE_OPTS[@]}" \
                             -U -Sy base-minimal bash bubblewrap \
                                    proxychains-ng ncurses python3

printf "repository=%s\n" "$REPO" >"$INSTALLDIR"/usr/share/xbps.d/00-repository-main.conf
