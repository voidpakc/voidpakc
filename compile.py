#!/usr/bin/env python3

import argparse
import os
import shutil
import signal
import sys
import tempfile

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/lib")

import common
import config
import glob
import iniparser
import proxy
import rundat
from common import ConfigError, bold_print as bprint
from runtime import Runtime
from squashfs import Squashfs

def searchbuild(name):
    SUFFIX = "/build.sh"
    try:
        return common.first_existing_of(
            [l + SUFFIX for l in [
                "scripts/" + name,
                name,
                os.path.dirname(name),
                "."]]) \
            [:-len(SUFFIX)]
    except FileNotFoundError:
        pass
    err = f"Could not find the build script for {name}\n" \
        "Build scripts are located in scripts/\n"

    fixup = {"supertuxkart": "stk", "extremetuxracer": "etr", "h-craft": "hcraft"}

    name = name.lower()
    if name in fixup: name = fixup[name]

    # TODO: Handle CWD != root
    for f in glob.glob("scripts/**/info", recursive=True):
        f = f[len("scripts/"):-len("/info")]
        if name in f:
            err += f"Did you mean '{f}'?\n"

    raise RuntimeError(err)

def parse_compinfo(fname):
    cfg = config._config_dict()
    section = "main"

    with open(fname) as f:
        for line in f:
            ns = iniparser.parse_section(line)
            if ns:
                section = ns
                continue

            key, value = iniparser.parse_entry(line, variables=cfg["main"])

            if not key:
                continue

            if key in cfg[section]:
                raise RuntimeError(f"Multiple entries for '{section}' / '{key}'")

            cfg[section][key] = value

        return cfg

class ArgumentParserWithGameList(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        if message:
            self._print_message(message, sys.stderr)
        if message and "GAME" in message:
            print("\nAvailable games to install:")

            installed = []
            not_installed = []

            image_dir = common.get_config("Paths", "images")

            for f in glob.glob("scripts/**/info", recursive=True):
                m = f[len("scripts/"):-len("/info")]
                g = m.split("/")[-1]

                if os.path.isdir(f"{image_dir}/src/{g}"):
                    installed += [m]
                else:
                    not_installed += [m]

            for g in installed:
                print(f"- {g} (installed)")

            for g in not_installed:
                print(f"- {g}")

        sys.exit(status)

def do_nothing(*args):
    pass

exit_fn = do_nothing

def create_file(name):
    open(name, "a")

def main():
    global exit_fn

    common.banner()

    optparse = ArgumentParserWithGameList()

    optparse.add_argument("--debug", action="store_true", help="open a shell in the compilation environment")
    group = optparse.add_mutually_exclusive_group()
    group.add_argument("--install", action="store_true", default=None, help="force installing")
    group.add_argument("--no-install", dest="install", action="store_false", help="disable installing")
    optparse.add_argument("--no-clean", action="store_true", help="keep the temporary directory after installation")
    optparse.add_argument("--compile-dir", metavar="DIR", help="use an existing compile directory")
    optparse.add_argument("game", metavar="GAME")

    cmd_args = optparse.parse_args()

    if cmd_args.install == None:
        cmd_args.install = not cmd_args.debug

    gamedir = searchbuild(cmd_args.game)
    game = os.path.basename(os.path.realpath(gamedir))

    bprint(f"Compiling {gamedir}\n")

    ci = parse_compinfo(f"{gamedir}/info")

    print(ci)

    download = {}

    src_dl = config.get_config("Paths", "source downloads")

    common.mkdir_parents(src_dl)

    src_dl = os.path.realpath(src_dl)

    sq = Squashfs()

    rt = Runtime("devel")
    rt.mount(sq)

    cdir = cmd_args.compile_dir

    if not cdir:
        cdir = tempfile.mkdtemp("", "compile-" + game + ".")

        for d in "tmp", "out":
            os.mkdir(f"{cdir}/{d}")

    args = [common.find_bwrap(), *common.bwrap_common_args(),
            "--unshare-all",
            "--ro-bind", rt.mnt, "/",
            "--proc", "/proc",
            "--dev", "/dev",
            "--tmpfs", "/var/tmp",
            "--tmpfs", "/usr/local",
            "--bind", cdir + "/tmp", "/tmp",
            "--bind", cdir + "/out", "/tmp/out",
            "--ro-bind", "void", "/tmp/scripts/void",
            "--ro-bind", "lib", "/tmp/scripts/lib",
            "--ro-bind", "scripts", "/tmp/scripts/scripts",
            "--ro-bind", "config-default.ini", "/tmp/scripts/config-default.ini",
            "--ro-bind-try", "config.ini", "/tmp/scripts/config.ini",
            "--ro-bind", gamedir, "/tmp/buildsc",
            "--dir", "/tmp/src",
            "--chdir", "/tmp/buildsc"
    ]

    dl = []
    dlsym = []

    if not cmd_args.compile_dir:
        for section in ci:
            if section.startswith("dl "):
                sect = ci[section]
                sha256sum = sect["SHA256SUM"].strip(" \t()")
                dl += ["--dl",
                       sect["URL"],
                       sha256sum,
                       section[3:]]
                dlsym += ["--symlink",
                          f"/tmp/dlf/{sha256sum}",
                          f"/tmp/dl/{section[3:]}"]

        if "PKG" in ci["main"]:
            for pkg in ci["main"]["PKG"].split():
                dl += ["--pkg", pkg]

    if not cmd_args.debug and not cmd_args.compile_dir:
        wrappers = []
        extra_args = []

        proxycfg = proxy.proxychains_conf()
        if proxycfg:
            print("Setting up proxy")

            wrappers += ["proxychains4"]

            f = common.tempfile_write(proxycfg)
            extra_args += ["--bind-data", str(f.fileno()), "/etc/proxychains.conf"]

        ret = common.run(args + extra_args + [
            "--share-net",
            "--bind", src_dl, "/tmp/dlf",
            "--", *wrappers,
            "python3", "/tmp/scripts/lib/source_download.py",
            *dl
        ])

        if ret:
            return 1

    args += ["--ro-bind", src_dl, "/tmp/dlf",
             *dlsym,
             "--", "bash"]
    if cmd_args.debug:
        args += ["--rcfile", "/tmp/scripts/void/cmpscripts.sh"]
    else:
        args += ["/tmp/scripts/void/cmpwrap.sh"]

    def debug_notice(*args):
        bprint(f"""

To enter the build environment for debugging run:

{sys.argv[0]} --debug --compile-dir {cdir} {cmd_args.game}

Add --install when you are ready to install the images.

""")
        return 1

    exit_fn = debug_notice

    caught_signal = False

    def handle_signal(*args):
        caught_signal = True

    signal.signal(signal.SIGINT, handle_signal)
    signal.signal(signal.SIGTERM, handle_signal)

    ret = common.run(args)

    if common.dry_run:
        for type_ in ("bin", "data"):
            create_file(f"{cdir}/out/{type_}.img")

    if (ret and not cmd_args.debug) or caught_signal:
        print(f"\n\nBuilding {game} failed!")
        return 1

    if not cmd_args.install:
        return 0

    image_dir = common.get_config("Paths", "images")

    for dir_ in ["bin", "data", "run/data", "src"]:
        common.mkdir_parents(image_dir + "/" + dir_)

    out_names = {"bin": game, "data": game}

    def check_rundat(file_, game):
        try:
            rd = rundat.parse(file_)
        except FileNotFoundError:
            return

        if rd["game"] != [game]:
            b = os.path.basename(file_)
            if len(rd["game"]) == 1:
                n = rd["game"][0]
            else:
                n = rd["game"]
            raise RuntimeError(f"Rundat '{b}' from '{game}' clashes with "
                               f"rundat '{file_}' from '{n}'")

    def copy_file(src, dst):
        dst = os.path.realpath(dst)
        print(f"Installing {src} to {dst}")
        shutil.copyfile(src, dst + ".tmp")
        shutil.move(dst + ".tmp", dst)


    # Install rundat files:

    rundat_list = [r for r in os.listdir(gamedir) if r.endswith(".rundat")]

    data = None

    for r in sorted(rundat_list):
        file_ = gamedir + "/" + r

        rd = rundat.parse(file_)

        if rd["game"] != [game]:
            raise RuntimeError(f"Game name {game} doesn't match with "
                               f"rundat {rd['game']}")

        type_ = rd["type"] or ["game"]

        outd = image_dir + ("/run/" if type_[0] == "game" else "/run/data/")

        outfile = os.path.realpath(outd + r)

        check_rundat(outfile, game)

        copy_file(file_, outfile)

        if data is False:
            continue

        if len(type_) != 2 or type_[0] != "game" \
           or (data and type_[1] != data):
            data = False
            continue

        data = type_[1]

    # Generate and install a data rundat if needed:

    # If there are both binary and data images, all game rundats
    # have the same single compatible game and there are no data
    # rundats, then generate a data rundat with the same name as
    # the listed compatible data set.

    if data and os.path.isfile(f"{cdir}/out/bin.img") \
       and os.path.isfile(f"{cdir}/out/data.img"):

        if not data.replace("-", "_").isidentifier() or \
           not data.isascii():
            raise RuntimeError("Game names should only contain alphanumeric "
                               "characters and dashes")

        out_names["data"] = data

        rd = "\n".join([
            f"game {data}",
            f"type data {data}",
            ""])

        rd_file = f"{image_dir}/run/data/{data}.rundat"

        check_rundat(rd_file, game)

        print(f"Installing generated data rundat to {rd_file}")

        with open(rd_file, "w") as f:
            f.write(rd)

    # Install the squashfs images:

    for type_ in out_names:
        name = out_names[type_]
        src = f"{cdir}/out/{type_}.img"

        if not os.path.isfile(src):
            continue

        mi = common.MountDirs(type_, name)

        if os.path.isdir(mi.unpacked):
            print(f"""

WARNING: An unpacked directory for {name} exists.

The installed image will not be used until the unpacked directory has
been moved or deleted:

    {mi.unpacked}

""")

        try:
            if len(os.listdir(mi.mnt)):
                print(f"""

WARNING: An older version of {name} is already mounted.

For the new version to be used, you will need to unmount the old image:

    umount -v {mi.mnt}

""")
        except FileNotFoundError:
            pass

        copy_file(src, mi.image)


    def install_src():
        # Install the compile scripts:
        src_install_path = f"{image_dir}/src/{game}"

        if os.path.isdir(src_install_path):
            try:
                shutil.rmtree(src_install_path)
            except:
                print("Giving up on installing compile scripts to "
                      f"{src_install_path}")
                return

        print(f"Installing compile scripts to {src_install_path}")

        shutil.copytree(gamedir, src_install_path)

    install_src()


    if cmd_args.no_clean:
        return 0

    exit_fn = do_nothing

    print(f"Cleaning up {cdir}")

    shutil.rmtree(cdir)

    return 0

if __name__ == "__main__":
    try:
        ret = main()
    # After ^C, the bwrap process might have already exited by the
    # time we handle the signal
    except ProcessLookupError:
        ret = 1
    finally:
        exit_fn()
    exit(ret)
