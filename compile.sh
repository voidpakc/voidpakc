#!/bin/bash

set -e

usage() {
    printf "USAGE: %s GAME\n" "$0"
}

if ! [ -e "$(basename "$0")" ] && ! [ -e bootstrap-void.sh ]; then
    printf "This script must be run in the repo directory\n"
    exit 1
fi

if ! [ "$1" ] || [ "${1:0:1}" = "-" ]; then
    usage
    exit 1
fi

if [ -e config ]; then
    . ./config
else
    printf "Could not find config file\n"
    exit 1
fi

for GAME in "scripts/$1" "$1" "$(dirname "$1")" ""
do
    if [ -e "$GAME"/build.sh ]; then
        break
    fi
done

if ! [ -e "$GAME" ]; then
    printf 'Could not find "%s"\n' "$1"
    exit 1
fi

GNAME="$(basename -- "$GAME")"

BWRAP="$IMAGE_DIR"/static/bwrap
if ! "$BWRAP" --version >/dev/null; then
    printf "Warning: Could not find static bwrap\n"
    BWRAP=bwrap
fi

#SQFS="$IMAGE_DIR"/static/squashfuse_ll
SQFS="$IMAGE_DIR"/static/squashfuse
if ! "$SQFS" --help |& grep -q foreground; then
    SQFS=squashfuse
    if ! "$SQFS" --help |& grep -q foreground; then
        printf "Unable to find a working squasfuse\n"
        exit 1
    fi
fi

#"$SQFS" --version |& head -n1

IMAGE="$(find "$IMAGE_DIR"/devel/ -name '*.img' | LC_ALL=C sort | tail -n1)"
RTM="$IMAGE_DIR"/mnt/devel-"$(basename -- "$IMAGE" .img)"
if ! [ -d "$RTM" ] || ! [ "$(ls "$RTM")" ]; then
    mkdir -p "$RTM"
    "$SQFS" "$IMAGE" "$RTM"
fi

if ! [ "$IMAGE" ]; then
    printf "Could not find runtime devel image in %s\n" "$IMAGE_DIR"/runtime-devel
    exit 1
fi

if ! [ "$SQMNT" ]; then
    SQMNT="$(mktemp -d --tmpdir compile-"$GNAME".XXXXXX)"

    printf "Temporary directory: %s\n" "$SQMNT"

    mkdir "$SQMNT"/{tmp,out}

    if [ "$SOCKS_PROXY" ]; then
        # TODO: Move this somewhere else so runner.sh can also use it
        PROXYURL="$(printf "%s" "$SOCKS_PROXY" | cut -d/ -f3 | cut -d: -f1)"
        PROXYPORT="$(printf "%s" "$SOCKS_PROXY" | cut -d/ -f3 | cut -s -d: -f2)"
        printf "strict_chain\nquiet_mode\nproxy_dns\ntcp_read_time_out 15000\ntcp_connect_time_out 8000\nlocalnet 127.0.0.0/255.0.0.0\n[ProxyList]\nsocks5 %s %s\n" "$PROXYURL" "$PROXYPORT" >"$SQMNT"/proxychains.conf
    fi
fi

PROXYCHAINS=
if [ "$SOCKS_PROXY" ]; then
        PROXYCHAINS=proxychains4
fi

if [ "$DEBUG" ]; then
    BUILD=( --rcfile /tmp/scripts/void/cmpscripts.sh )
else
    BUILD=( /tmp/scripts/void/cmphelp.sh )
fi

ARG1="$1"
debug_notice() {
    printf "\n\nTo enter the build environment for debugging run\n"
    printf "DEBUG=1 SQMNT=%s ./compile.sh %s\n\n\n" "$SQMNT" "$ARG1"
    printf "Add INSTALL=1 when you are ready to install the images.\n"
}

trap "{ debug_notice; exit -2; }" SIGINT SIGTERM

mkdir -p "$SRCDL"

# TODO:
# Allow setting the runtime to a RW one for debugging (maybe
# automatically doing an overlay mount?)

set +e
env - \
"$BWRAP" --unshare-all --share-net --ro-bind "$RTM" / \
         --setenv TERM "$TERM" \
         --setenv PATH /usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/sbin:/bin \
         --setenv CFPATH /tmp/scripts/ \
         --setenv LANG "$(printf "%s" "$LOCALES" | cut -d" " -f1)" \
         --setenv LC_COLLATE C \
         --proc /proc --dev /dev --tmpfs /var/tmp \
         --ro-bind-try "$SQMNT"/proxychains.conf /etc/proxychains.conf \
         --bind "$SQMNT"/tmp /tmp --ro-bind . /tmp/scripts \
         --bind "$SRCDL" /tmp/dlf --bind "$SQMNT"/out /tmp/out \
         --tmpfs /usr/local \
         --dir /tmp/src \
         --chdir /tmp/scripts/"$GAME" \
         -- $PROXYCHAINS \
         bash "${BUILD[@]}"

RV=$?
set -e

if ! [ "$DEBUG" = 1 ] && ! [ "$RV" = 0 ]; then
    printf "Building %s failed\n" "$1"
    debug_notice
    exit 1
fi

if [ "$DEBUG" = 1 ] && ! [ "$INSTALL" = 1 ]; then
    debug_notice
    exit 0
fi

mkdir -p "$IMAGE_DIR"/{bin,run,data,src}

PREEXIST=
if [ "$(ls "$IMAGE_DIR"/mnt/bin-"$GNAME"/ 2>/dev/null)" ]; then
    PREEXIST=1
    UMOUNT_BIN=1
fi
if [ "$(ls "$IMAGE_DIR"/mnt/data-"$GNAME"/ 2>/dev/null)" ]; then
    PREEXIST=1
    UMOUNT_DATA=1
fi

symmv() {
    if [ -e "$1" ]; then
        mv -v -- "$1" "$(realpath -- "$2")"
    fi
}

symmv "$SQMNT"/out/bin.img "$IMAGE_DIR"/bin/"$GNAME".img
symmv "$SQMNT"/out/share.img "$IMAGE_DIR"/data/"$GNAME".img
symmv "$SQMNT"/out/data.img "$IMAGE_DIR"/data/"$GNAME".img
cp -v "$GAME"/*rundat "$IMAGE_DIR"/run ||:
rm -rv "$IMAGE_DIR"/src/"$GNAME" ||:
cp -rv "$GAME" "$IMAGE_DIR"/src

if [ "$PREEXIST" ]; then
    printf "\n\nWARNING: An older version of %s is already mounted.\n" "$GNAME"
    printf "For the new version to be used, you will need to unmount the old\n"
    printf "version with:\n\n    umount -v"
    if [ "$UMOUNT_BIN" = 1 ]; then
        printf " %s" "$IMAGE_DIR"/mnt/bin-"$GNAME"
    fi
    if [ "$UMOUNT_DATA" = 1 ]; then
        printf " %s" "$IMAGE_DIR"/mnt/data-"$GNAME"
    fi
    printf "\n\n"
fi

if [ "$NOCLEAN" = 1 ]; then
    debug_notice
    exit
fi

printf "Cleaning up.\n"
rm --one-file-system -rf "$SQMNT"
