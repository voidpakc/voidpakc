#!/bin/bash

set -e

usage() {
    printf "USAGE: %s PROGRAM [ARGS...]\n" "$0"
    printf "If the environment variable ROOTDIR is not set,\nVOID_ROOT_DIR from the config file is used.\n"
    printf "This script defaults to running bash if PROGRAM is not specified\n"
}

if ! [ -e "$(basename "$0")" ] && ! [ -e bootstrap-void.sh ]; then
    printf "This script must be run in the repo directory\n"
    exit 1
fi

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    usage
    exit 1
fi

eval $(python3 ./lib/config.py --env \
               --get "Container" "socks proxy" \
               --get "Void" "bootstrap" \
               --get "Void" "locales" \
               --get "Void" "package cache" \
    )

if [ "$?" != 0 ]; then
    printf "The config system is broken\n"
    printf "Maybe you don't have a new enough python3\n"
    exit 1
fi

if [ "$ROOTDIR" ]; then
    ROOT="$ROOTDIR"
else
    ROOT="$BOOTSTRAP"
fi

if [ "$1" ]; then
    PROG="$1"
    shift 1
else
    PROG=bash
fi

SCRIPTSDIR="$(pwd)"

PKGCACHE_DIR="$(realpath -m -- "$PACKAGE_CACHE")"

cd "$ROOT"

if [ "$NO_BWRAP" != 1 ]; then
    if [ "$PKGCACHE_DIR" ]; then
        BWRAP_CACHE_OPTS=( --dir /var/cache/xbps --bind "$PKGCACHE_DIR" /var/cache/xbps )
    fi

    # TODO: How safe are we to assume the interpreter will be in the first kilobyte?
    INTERPRETER="./$(head -c1024 ./bin/bwrap | tr '\0\n' '\n\0' | grep -a '/lib/ld-' | head -n1)"
    # TODO: Prevent infinite looping
    while [ -L "$INTERPRETER" ]
    do
        INTERPRETER="$(realpath "$INTERPRETER")"
        INTERPRETER="./${INTERPRETER#$(pwd)}"
    done

    env - \
        PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/sbin:/bin \
        LD_LIBRARY_PATH=./lib \
        TERM="$TERM" \
        SOCKS_PROXY="$SOCKS_PROXY" \
        LANG="$(printf "%s" "$LOCALES" | cut -d" " -f1)" \
        LC_COLLATE=C \
        "$INTERPRETER" \
        ./bin/bwrap --unshare-all --share-net --bind . / --dir /scripts \
        --ro-bind "$(realpath /etc/resolv.conf)" /etc/resolv.conf \
        --ro-bind "$SCRIPTSDIR" /scripts --proc /proc --dev /dev \
        --setenv LD_LIBRARY_PATH "" \
        "${BWRAP_CACHE_OPTS[@]}" \
        --setenv CHROOT_MODE bwrap \
        "$PROG" "$@"
else
    printf "WARNING: Bad things can happen when using chroots.\n"
    printf "This should only be used for running in containers\n"
    printf "where nested namespaces are disabled.\n\n"
    printf "In particular, be very careful about moving or deleting\n"
    printf "anything, as you could accidentally remove your files\n"
    printf "or device nodes.\n"
    if hash tput 2>/dev/null; then
        tput bel ||:
    fi
    if ! [ "$IS_CI" = 1 ]; then
        sleep 15
    fi
    do_mount() {
        # TODO: Add a check to not mount the same thing multiple times...
        mkdir -p "$2"
        mount -R "$1" "$2"
    }
    do_mount /dev dev
    do_mount /sys sys
    do_mount /proc proc
    mkdir -p scripts
    do_mount "$SCRIPTSDIR" scripts
    if [ "$PKGCACHE_DIR" ]; then
        do_mount "$PKGCACHE_DIR" var/cache/xbps
    fi
    cp /etc/resolv.conf etc/resolv.conf
    CHROOT_MODE=chroot chroot . "$PROG" "$@"
fi

