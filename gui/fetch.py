#!/usr/bin/env python3

from os import listdir

from config import get_config, read_config
read_config()

EXT = ".rundat"

RUNDIR = get_config("Paths", "images") + "/run/"

try:
    rundat_list = listdir(RUNDIR)
except FileNotFoundError:
    rundat_list = []
rundat_list.sort()

class ProgData:
    """Information from a program / data rundat"""

    def __init__(self):
        self.type = "game"
        self.use = []
        self.compat = []

entries = []
propdict = {}
datadict = {}

for f in rundat_list:
    if not f.endswith(EXT):
        continue
    name = f[:-len(EXT)]
    path = RUNDIR + f
    file = open(path)

    p = ProgData()

    for line in file.readlines():
        d = line.strip("\n").split(" ")
        if not len(d):
            continue
        directive = d[0]
        args = d[1:]

        if directive == "use":
            p.use = args
        elif directive == "type":
            if not len(args):
                continue
            p.type = args[0]
            p.compat = args[1:]

    propdict[name] = p
    if p.type == "game":
        entries += [name]
    else:
        for x in p.compat:
            if x in datadict:
                datadict[x] += [name]
            else:
                datadict[x] = [name]

    file.close()
