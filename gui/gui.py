#!/usr/bin/env python3

import sys
import os

os.chdir(os.path.dirname(os.path.realpath(__file__)) + "/..")
sys.path.insert(0, "lib")

from PyQt5.QtWidgets import *
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt

from window import app, window, close

from pg_launch import launchpage
from pg_config import configpage

escape = QShortcut(QKeySequence(Qt.Key_Escape), configpage)
escape.activated.connect(close)

topbar = QListWidget()
topbar.setFlow(QListWidget.LeftToRight)
topbar.setFixedHeight(92)

topbar.addItems(["Config", "Launch"])

cw = configpage

def gosection(item):
    global cw
    cw.hide()
    s = item.text()
    if s == "Config":
        cw = configpage
    else:
        cw = launchpage
    cw.show()

topbar.currentItemChanged.connect(gosection)

layout = QVBoxLayout()
layout.addWidget(topbar)

for w in launchpage, configpage:
    layout.addWidget(w)
    w.hide()

window.setLayout(layout)
window.show()

cw.show()

app.exec_()
