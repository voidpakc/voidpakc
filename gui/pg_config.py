from PyQt5.QtWidgets import *
from PyQt5.QtGui import QKeySequence, QFont, QFontMetrics
from PyQt5.QtCore import pyqtSignal as Signal, pyqtSlot as Slot
from PyQt5.QtCore import Qt, QObject

import config

import window

configpage = QWidget()

top = QHBoxLayout()

close = QPushButton("Close")
close.clicked.connect(window.close)

def do_save():
    print("Saving config")
    config.save_config()

save = QPushButton("Save")
save.clicked.connect(do_save)

enter = QShortcut(QKeySequence(Qt.Key_Return), configpage)
enter.activated.connect(do_save)

bm = QHBoxLayout()
bm.addStretch(1)
bm.addWidget(close)
bm.addWidget(save)

body = QGridLayout()

heading_font = QFont()
heading_font.setPointSize(heading_font.pointSize() * 1.5)

layout = QVBoxLayout()
layout.addLayout(top)
layout.addLayout(body)
layout.addLayout(bm)

configpage.setLayout(layout)

def build_cap_fix_list():
    words = ["gl4es", "SOCKS"]

    fix = {}

    for word in words:
        lc = word.lower()
        tc = lc.capitalize()

        fix[lc] = word
        fix[tc] = word

    return fix

cap_fix_list = build_cap_fix_list()

def fix_caps(text):
    new = []
    for word in text.split(" "):
        if word in cap_fix_list:
            new += [cap_fix_list[word]]
        else:
            new += [word]
    return " ".join(new)

class ConfigUpdater(QObject):
    @Slot()
    def lineEdit(self):
        s = self.sender()
        self.set_cfg(s.text(), s)

    def set_cfg(self, text, meta):
        config.set_config(meta.section, meta.key, text)

cfg = ConfigUpdater()

def get_dir():
    return QFileDialog.getExistingDirectory()

class PathLine(QLineEdit):
    @Slot()
    def select_path(self):
        self.setText(get_dir())
        self.editingFinished.emit()

def make_path_line(val, section="", key=""):
    l = QHBoxLayout()
    line = PathLine(val)
    line.section = section
    line.key = key
    line.editingFinished.connect(cfg.lineEdit)
    button = QPushButton("Path")
    button.clicked.connect(line.select_path)
    l.addWidget(line)
    l.addWidget(button)
    return l

row = 0

for section in config.defconfig:
    sectheading = QLabel()
    sectheading.setText(section)
    sectheading.setFont(heading_font)
    sectheading.setFixedHeight(QFontMetrics(heading_font).height() * 1.5)
    sectheading.setAlignment(Qt.AlignBottom)
    body.addWidget(sectheading, row, 0)
    row += 1
    for item in config.defconfig[section]:
        item_w = QLabel()
        item_w.setText(fix_caps(item.capitalize()) + ":")
        item_w.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        body.addWidget(item_w, row, 0)

        val = config.get_config(section, item, new=True)
        meta = config.get_config(section, item, default=True, meta=True)[1]

        if "type" in meta:
            item_type = meta["type"]
        else:
            item_type = "str"

        print(item_type)

        is_widget = True

        if item_type == "str":
            c_e = QLineEdit(val)

            c_e.section = section
            c_e.key = item
            c_e.editingFinished.connect(cfg.lineEdit)
        elif item_type == "dir":
            is_widget = False
            c_e = make_path_line(val, section, item)
        else:
            row += 1
            continue

        if is_widget:
            body.addWidget(c_e, row, 1)
        else:
            body.addLayout(c_e, row, 1)
        row += 1
