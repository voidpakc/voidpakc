from PyQt5.QtWidgets import *
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt

from fetch import entries, propdict, datadict

import window

launchpage = QWidget()

datalist = QListWidget()
uselist = QListWidget()

def plclicked(item):
    name = item.text()
    info = propdict[name]

    uselist.clear()

    for x in info.use:
        li = QListWidgetItem(x)
        li.setFlags(item.flags() | Qt.ItemIsUserCheckable)
        li.setCheckState(Qt.Checked)
        uselist.addItem(li)

    datalist.clear()
    for x in info.compat:
        if x in datadict:
            for y in datadict[x]:
                datalist.addItem(y)
    datalist.setCurrentRow(0)

plist = QListWidget()
plist.currentItemChanged.connect(plclicked)
plist.addItems(entries)
plist.setCurrentRow(0)

top = QHBoxLayout()
top.addWidget(plist)
top.addWidget(datalist)
top.addWidget(uselist)

close = QPushButton("Close")
close.clicked.connect(window.close)

def do_launch():
    prog = plist.currentItem().text()
    print(prog)
    if datalist.count():
        data = datalist.currentItem().text()
        print(data)
    for i in range(uselist.count()):
        x = uselist.item(i)
        if x.checkState() == 0:
            print(x.text())
    window.close()

launch = QPushButton("Launch")
launch.clicked.connect(do_launch)

enter = QShortcut(QKeySequence(Qt.Key_Return), launchpage)
enter.activated.connect(do_launch)

bm = QHBoxLayout()
bm.addStretch(1)
bm.addWidget(close)
bm.addWidget(launch)

launchlayout = QVBoxLayout()
launchlayout.addLayout(top)
launchlayout.addLayout(bm)

launchpage.setLayout(launchlayout)
