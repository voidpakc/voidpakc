#!/bin/bash

set -e

usage() {
    printf "USAGE: %s [ROOTDIR]\n" "$0"
    printf "If ROOTDIR is not specified, \"bootstrap\" from the config file is used.\n"
}

if [ "${1:0:1}" = "-" ]; then
    usage
    exit 1
fi

if ! [ -e "$(basename "$0")" ] && ! [ -e bootstrap-void.sh ]; then
    printf "This script must be run in the repo directory\n"
    exit 1
fi

if ! python3 ./lib/config.py --get Paths images >/dev/null; then
    printf "The config system is broken\n"
    printf "Maybe you don't have a new enough python3\n"
    exit 1
fi


PKGCACHE_DIR="$(realpath -m -- "$(python3 ./lib/config.py --get Void "package cache")")"

IMAGE_DIR="$(realpath -m -- "$(python3 ./lib/config.py --get Paths images)")"

if [ "$1" ]; then
    VOID_ROOT_DIR="$1"
    export ROOTDIR="$1"
else
    VOID_ROOT_DIR="$(python3 ./lib/config.py --get Void bootstrap)"
fi

if ! [ -d "$VOID_ROOT_DIR" ]; then
    printf "The Void root '%s', does not exist or\n" "$VOID_ROOT_DIR"
    printf "is not a directory\n"
    exit 1
fi

if ! [ "$PKGCACHE_DIR" ]; then
    PKGCACHE_DIR=var/cache/xbps
fi

# The install script can't bind the cache (no root) so we have to do it here:
# TODO: Fix architecture detection
VOID_ARCH="$(grep -r architecture "$VOID_ROOT_DIR"/usr/share/xbps.d/ | cut -d= -f2 | head -n1)"
if ! [ "$VOID_ARCH" ]; then
    if grep -qr aarch64 "$VOID_ROOT_DIR"/usr/share/xbps.d; then
        VOID_ARCH=aarch64
    else
        VOID_ARCH=x86_64
    fi
fi

if [ "$NO_BWRAP" != 1 ]; then
    ENTER_OPTS=( --bind "$PKGCACHE_DIR" /tmp/void-packages-master/hostdir/repocache-"$VOID_ARCH" )
else
    CM="$VOID_ROOT_DIR"/tmp/void-packages-master/hostdir/repocache-"$VOID_ARCH"
    mkdir -p "$CM"
    mount -B "$PKGCACHE_DIR" "$CM"
    ENTER_OPTS=( )
fi

run() {
    ./enter-void.sh "${ENTER_OPTS[@]}" "$@"
}

run /scripts/void/setup-void.sh
run /scripts/void/setup-runtime.sh

mkdir -p "$IMAGE_DIR"/{rt,devel,static}
DATE="$(date -I)"
# TODO: Handle collisions
mv -v "$VOID_ROOT_DIR"/tmp/runtime.img "$IMAGE_DIR"/rt/"$DATE".img
mv -v "$VOID_ROOT_DIR"/tmp/runtime-devel.img "$IMAGE_DIR"/devel/"$DATE".img
cp -av "$VOID_ROOT_DIR"/tmp/static/* "$IMAGE_DIR"/static ||:

set +e

UM=
UM_R=
UM_D=
if [ -e "$IMAGE_DIR"/mnt/rt-"$DATE"/usr ]; then
    UM=1
    UM_R=1
fi
if [ -e "$IMAGE_DIR"/mnt/devel-"$DATE"/usr ]; then
    UM=1
    UM_D=1
fi

if [ "$UM" ]; then
    printf "\n\nWARNING: Older runtime versions are still mounted.\n"
    printf "The new images will not be used (and the space of the old ones will\n"
    printf "not be freed) until you unmount them.\n"
    printf "To do so, run:\n\n"
    printf "    umount -v"
    [ "$UM_R" ] && printf " %s" "$IMAGE_DIR"/mnt/rt-"$DATE"
    [ "$UM_D" ] && printf " %s" "$IMAGE_DIR"/mnt/devel-"$DATE"
    printf "\n\n\n"
fi
