import os
import subprocess
import tempfile
from shutil import which
from shlex import quote

from config import *

class CommandNotFoundError(RuntimeError):
    pass

dry_run = os.name == "nt" or os.getenv("DRY_RUN")

def banner():
    """Print a banner with version information."""

    print("voidpakc 0.1 (c) 2020 Icecream95 <ixn@disroot.org>\n", flush=True)

def env_dict(env_list):
    """Create a dictionary of environment variables.

    This function returns a dictionary that can be used by
    subprocess.run containing a mapping of the environment variables
    specified in env_list to their current values.

        >>> env_dict(["PATH"])
        {"PATH": "/usr/local/bin:/usr/bin"}

    """

    env = {}
    for f in env_list:
        e = os.getenv(f)
        if e != None:
            env[f] = e

    return env

def minimal_env(env_pass=[]):
    """Return a minimal environment dictionary.

    This function returns an environment dictionary for use by
    subprocess.run with only PATH and LD_LIBRARY_PATH set.

    """

    return env_dict(["PATH", "LD_LIBRARY_PATH", *env_pass])

def run(cmd, fd_map={}, log=True, args={}, env_pass=[]):
    if log:
        here_l = [str(fd) + "<<<" + quote(fd_map[fd]).replace("\n", "'$'\\n''") for fd in fd_map]
        print("$", " ".join(map(quote, cmd)), " ".join(here_l))

    if dry_run:
        print("(Not running command)")
        return 0

    return subprocess.run(cmd, env=minimal_env(env_pass), close_fds=False, **args).returncode

def first_existing_of(args, dir=False, err=FileNotFoundError()):
    """Return the first existing file of a list.

    If none of the paths exist, err (which defaults to
    FileNotFoundError) will be raised.

    """

    c = os.path.isdir if dir else os.path.isfile
    for f in args:
        if c(f):
            return f
    raise err

def find_rundat(game, data=False):
    """Search for a rundat

    """

    rp = "/run/data/" if data else "/run/"
    try:
        return first_existing_of([
            get_config("Paths", "images") + rp + game + ".rundat",
            game + ".rundat",
            game + "/" + game.split("/")[-1] + ".rundat",
            get_config("Paths", "images") + rp + game,
            game,
            ])
    except FileNotFoundError:
        return None

def mkdir_parents(d):
    """Create a directory and its parents as needed.

    This is similar to mkdir --parents.

    """

    if os.path.isdir(d):
        return
    mkdir_list = [d]
    while len(mkdir_list):
        try:
            m = mkdir_list[-1]
            os.mkdir(m)
            mkdir_list.pop()
        except FileNotFoundError:
            mkdir_list += [os.path.dirname(m)]
        except FileExistsError:
            return

def run_grep(cmd, regex):
    print("$", " ".join(cmd), "|& grep -e", quote(regex))
    env = minimal_env()
    p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=env)
    p2 = subprocess.Popen(["grep", "-e", regex], stdin=p1.stdout, env=env)

    return p2.wait()

def find_st_exec(name, test=["--version"], *, ret=0, grep="", doublecheck=False):
    for prog in [get_config("Exec", name),
                 get_config("Paths", "images") + "/static/" + name,
                 which(name)]:
        if not prog or not os.path.exists(prog):
            continue
        cmd = [prog, *test]
        if grep:
            actual_ret = run_grep(cmd, grep)
        else:
            actual_ret = run(cmd)
        if doublecheck:
            if run([prog, "--version"], log=False, args={"stdout": subprocess.DEVNULL}):
                continue
        if dry_run or actual_ret == ret:
            print("Using", prog)
            return prog
    raise CommandNotFoundError("Could not find " + name)

def find_bwrap():
    """Return the location of bwrap"""

    return find_st_exec("bwrap", [":"], ret=1, grep="new namespace", doublecheck=True)

def find_squashfuse():
    return find_st_exec("squashfuse", ["--help"], grep="^squashfuse")

def join_path_checked(base, path):
    joined = os.path.realpath(f"{base}/{path}")

    if os.path.relpath(joined, os.path.realpath(base)) \
              .startswith(".."):
        raise RundatError(f"{base}/{path} is a symlink to outside {base}")

    return joined

def check_image_type(imtype):
    if imtype not in ["rt", "devel", "bin", "data"]:
        raise RuntimeError("Unknown image type " + imtype)

def get_unpacked_dir(imtype, name):
    check_image_type(imtype)

    path = f"{get_config('Paths', 'images')}/{imtype}/{name}"

    return path

def get_image_path(imtype, name):
    check_image_type(imtype)

    path = f"{get_config('Paths', 'images')}/{imtype}/{name}.img"

    return path

def get_mnt_dir(imtype, name):
    check_image_type(imtype)

    path = f"{get_config('Paths', 'images')}/mnt/{imtype}-{name}"

    return path

class MountDirs:
    def __init__(self, imtype, name):
        self.unpacked = get_unpacked_dir(imtype, name)
        self.image = get_image_path(imtype, name)
        self.mnt = get_mnt_dir(imtype, name)

def select(prompt, items):
    num = len(items)
    prompt = "\n".join(["", prompt] + [f"{i}) {items[i]}" for i in range(num)])

    while True:
        print(prompt)

        try:
            selected = int(input("#? "))

            if selected >= 0 and selected < num:
                return selected

        except ValueError:
            pass

        print(f"\nInput a number in the range 0-{len(items)-1}")

    print(selected)

def tempfile_write(text, cloexec=False):
    f = tempfile.TemporaryFile(mode="w+", newline="\n")
    f.write(text)
    f.seek(0)

    if not cloexec:
        os.set_inheritable(f.fileno(), True)

    return f

def bold_print(text, **kwargs):
    """Print text in a bold style."""

    print(f"\033[1m{text}\033[0m", **kwargs)

def bwrap_common_args():
    return ["--setenv", "TERM", os.getenv("TERM"),
            "--setenv", "PATH", "/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/sbin:/bin",
            "--setenv", "LANG", "en_US.UTF-8",
            "--setenv", "LC_COLLATE", "C",
            "--setenv", "HOME", "/tmp/home",
            "--setenv", "CFPATH", "/tmp/scripts/",
            "--setenv", "XDG_RUNTIME_DIR", "/tmp/run"]

read_config()
