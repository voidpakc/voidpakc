import collections
import os
import shlex

import iniparser
from iniparser import ConfigError

CFGDIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

CFG = f"{CFGDIR}/config.ini"
DEFCFG = f"{CFGDIR}/config-default.ini"

def _config_dict():
    """Return a new config dictionary.

    This is just a defaultdict of dict.

    Using a constant doesn't work as assinging it won't do a deep
    copy, so a function needs to be used.

    """

    return collections.defaultdict(dict)

defconfig = _config_dict()
config = _config_dict()
newconf = _config_dict()

def _parse_config(filename, parse_meta=False):
    """Parse a config file.

    Takes a filename as an argument.

    Returns a nested dictionary of sections to entries.

    config["Foo"]["bar"] will give the value of entry "bar" in section
    "Foo".

    """

    config = _config_dict()

    with open(filename) as f:
        current_section = ""

        meta = {}

        for line in f:

            new_section = iniparser.parse_section(line)
            if new_section:
                current_section = new_section
                continue

            [key, value] = iniparser.parse_entry(line)

            if not key:
                if parse_meta and "#" in line:
                    iniparser.parse_comment(meta, line)

                continue

            if current_section == "":
                raise ConfigError("Config entry before a section")

            if key in config[current_section]:
                raise ConfigError(f"Duplicate entry '{current_section}' / '{key}'")

            config[current_section][key] = [value, meta]
            meta = {}

    return config

def read_config():
    """Read the config files.

    Reads from DEFCFG into the global variable `defconfig', and from
    CFG (if it exists) into the global variable `config'.

    """

    global defconfig, config
    defconfig = _parse_config(DEFCFG, parse_meta=True)
    try:
        config = _parse_config(CFG)
    except FileNotFoundError:
        config = _config_dict()

def _append_config(config, section, key, value):
    """Append to a config file.

    Arguments:
    config -- a list of lines making up the config file
    section }
    key     } the entry to insert into the config file
    value   }

    Returns the updated config file.

    """

    line_to_insert = f"{key} = {shlex.quote(value)}\n"
    new_config = []
    current_section = ""
    blank_lines = 0
    inserted = False

    def flush_blank_lines(reset=True):
        nonlocal new_config
        nonlocal blank_lines
        new_config += ["\n" for i in range(blank_lines)]
        if reset:
            blank_lines = 0

    for line in config:
        if not line.strip():
            blank_lines += 1
            continue

        new_section = iniparser.parse_section(line)
        if new_section:
            if current_section == section and not inserted:
                new_config += [line_to_insert]
                inserted = True
                # We want at least one blank line between the new
                # config entry and the next section.
                blank_lines = max(blank_lines, 1)

            current_section = new_section

        flush_blank_lines()

        new_config += [line]

    flush_blank_lines(reset=False)

    if not inserted:
        if current_section != section:
            if new_config and not blank_lines:
                new_config += ["\n"]
            new_config += [f"[{section}]\n"]

        new_config += [line_to_insert]

    return new_config

class MissingEntryError(RuntimeError):
    pass

class DuplicateEntryError(RuntimeError):
    pass

def _update_config(config, section, key, value):
    """Updates an entry in a config file.

    Arguments:
    config -- a list of lines making up the config file
    section }
    key     } the entry to update in the config file
    value  -- the new value for that entry

    Returns the updated config file.

    """

    new_config = []
    current_section = ""
    updated = 0

    for line in config:
        new_section = iniparser.parse_section(line)
        if new_section:
            current_section = new_section
            new_config += [line]
            continue

        current_key = iniparser.parse_entry(line)[0]

        if [current_section, current_key] == [section, key]:
            updated += 1
            new_config += [f"{key} = {shlex.quote(value)}\n"]
        else:
            new_config += [line]

    if not updated:
        raise MissingEntryError()
    elif updated > 1:
        raise DuplicateEntryError()

    return new_config

def _change_config(cfg, section, key, value):
    """Change (update or append) to a config file

    Arguments:
    config -- a list of lines making up the config file
    section }
    key     } the entry to set in the config file
    value   }

    If the entry already exists, it will be updated with the new
    value, otherwise it will be appended to the config file.

    Returns the updated config file.

    """

    try:
        cfg = _update_config(cfg, section, key, value)
    except MissingEntryError:
        cfg = _append_config(cfg, section, key, value)
    return cfg

def get_config(section, key, new=False, require=True, meta=False, default=False):
    """Retrieves a configuration entry.

    Arguments:
    section  }
    key      } the entry to retrieve from the config file
    new     -- if True, values may be retrieved from the staging area
               (configuration not yet written to disk)
    require -- if True (default), an error will be raised if the config entry
               is not present in the default config file
    meta    -- if True, return metadata for the config entry
    default -- only return data from the default configuration

    If require is False then None is returned when the entry does not
    exist in the configuration store.

    Otherwise, a string is returned.

    For example, for a config file

        [Foo]
        bar = baz

    get_config("Foo", "bar") would return "baz".

    """

    if meta:
        get = lambda x: x
    else:
        get = lambda x: x[0]

    if require:
        if not key in defconfig[section]:
            raise ConfigError("No default value for config entry" \
                              f" '{section}' / '{key}'")

    if new:
        if key in newconf[section]:
            return get(newconf[section][key])

    if not default and key in config[section]:
        return get(config[section][key])

    if not require:
        if not key in defconfig[section]:
            return get([None])

    return get(defconfig[section][key])

def set_config(section, key, value):
    """Sets a configuration entry.

    Arguments:
    section }
    key     } the entry to set in the config file
    value   }

    This only updates the configuration staging area, a save_config()
    is required to actually save the change to disk.

    If the entry does not exist in the default config file, an error
    is raised.

    """

    # Make sure we have a default value for the config entry
    get_config(section, key, require=True)

    newconf[section][key] = value

def save_config():
    """Writes the configuration staging area to disk.

    """

    global newconf
    cfg = None
    try:
        with open(CFG) as f:
            cfg = list(f)
    except FileNotFoundError:
        cfg = []

    for section in sorted(newconf):
        for key in sorted(newconf[section]):
            value = newconf[section][key]

            if get_config(section, key, new=False, require=True) == value:
                continue

            config[section][key] = value

            cfg = _change_config(cfg, section, key, value)

    newconf.clear()

    with open(CFG, "w") as f:
        f.write("".join(cfg))

if __name__ == "__main__":
    import argparse

    optparse = argparse.ArgumentParser(description="Get or set values from the config file.",
                                     epilog=f"The config files read are {CFG} and {DEFCFG}.")
    optparse.add_argument("--env", action="store_true", help="Print as environment variables")
    group = optparse.add_mutually_exclusive_group()
    group.add_argument("--meta", action="store_true", help="Print config metadata")
    group.add_argument("--get", nargs=2, metavar=("SECT", "KEY"), action="append",
                       help="get a config value")
    group.add_argument("--set", nargs=3, metavar=("SECT", "KEY", "VAL"),
                       help="set and save a config value")

    args = optparse.parse_args()

    if args.get and len(args.get) > 1 and not args.env:
        optparse.error("--get can only be specified multiple times with --env")

    if args.env and not args.get:
        optparse.error("--env can only be used with --get")

    read_config()

    if args.get:
        if args.env:
            for section, key in args.get:
                env = key.upper().replace(" ", "_")
                value = get_config(section, key)
                print(f"{env}={shlex.quote(value)}")
        else:
            print(get_config(*args.get[0]))
    elif args.set:
        set_config(*args.set)
        save_config()
    else:
        for conf, name in [[defconfig, "Default"], [config, "User"]]:
            print(f"{name} config:")
            for section in sorted(conf):
                for key in sorted(conf[section]):
                    if args.meta:
                        print(f"'{section}' / '{key}': {conf[section][key]}")
                    else:
                        print(f"'{section}' / '{key}': '{conf[section][key][0]}'")
