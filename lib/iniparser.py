import os
import re
import shlex

class ConfigError(RuntimeError):
    pass

def parse_section(line):
    """Parse an INI section.

    Returns the section name if the line denotes an INI section
    (e.g. "Foo" for "[Foo ]\n"), otherwise returns None.

    """

    line = line.strip()

    if not line:
        return None

    if line[0] == "[" and line[-1] == "]":
        # Strip the brackets, then any whitespace inside
        return line.strip("[]").strip()

    return None

def _expand_variables(string, variables):
    """Expand variables.

    Returns string, after replacing variable references with values
    from the variables dictionary.

        >>> _expand_variables("$foo.${bar}baz", {"foo": "1", "bar": "2"})
        "1.2baz"

    """

    split = string.split("$")
    expanded = split[0]

    for exp in split[1:]:
        # Don't expand \$
        # endswith?
        if expanded and expanded[-1] == "\\":
            expanded = expanded[:-1] + exp
            continue

        if not exp:
            raise ConfigError("Incomplete variable expansion")

        if exp[0] == "{":
            var, rest = exp[1:].split("}", maxsplit=1)
        else:
            match = re.search(r"[^a-zA-Z0-9_]", exp)
            if match:
                pos = match.span()[0]
                var, rest = exp[:pos], exp[pos:]
            else:
                var, rest = exp, ""

        if not var in variables:
            raise ConfigError(f"Variable {var} not defined")

        expanded += variables[var] + rest

    return expanded

def parse_entry(line, variables=None):
    """Parse an INI entry.

    Returns a list of [key, value] if the line contains an entry,
    otherwise [None, None].

    For example, ["foo", "bar baz"] is returned for " foo   = bar baz"

    """

    # Is this a comment / blank line?
    if not line.split("#")[0].strip():
        return [None, None]

    try:
        # Split at "=" and strip surrounding whitespace
        [key, value] = map(str.strip, line.split("=", maxsplit=1))

        value = os.path.expanduser(value)

        if variables:
            value = value.replace("\\$", "\\\\$")

        # Strip comments
        value = " ".join(shlex.split(value, comments=True))

        if variables:
            value = _expand_variables(value, variables)

        if key:
            return [key, value]

    except ValueError:
        pass

    line = line.strip("\n")
    raise ConfigError(f"Malformed config entry '{line}'")

def parse_comment(meta, line):
    """Parse a comment line.

    Arguments:
    meta    -- a dictionary to write parsed metadata to
    line    -- a comment line from a config file

    The comments can be formatted like this:

        #! Whether to - comment line
        #> do foo     - continuation of comment line
        #%  code block
        #$path        - type
        #@Foo/bar=1   - disable unless this other var is set
        #?commented line = example

    The keys used for indexing the metadata are "comment", "type" and
    "dep".

    The "dep" entry is itself a dictionary, with keys being
    "{Section}/{entry}", and values being the required value.

    """

    if len(line) < 2 or line[0] != "#":
        return

    ctrl = line[1]
    rest = line[2:].strip()

    if ctrl in {"!", ">", "%"}:
        if not "comment" in meta:
            meta["comment"] = []
        if ctrl == "!":
            meta["comment"] += [rest]
        elif ctrl == "%":
            meta["comment"] += ["    " + rest]
        else:
            if not len(meta["comment"]):
                raise ConfigError("Line continuation (#>) before" \
                                  "description(#!) in config")
            # Append the line to the last line in the comment array
            ov = meta["comment"]
            meta["comment"] = ov[:-1] + [ov[-1] + " " + rest]
    elif ctrl == "$":
        if "type" in meta:
            raise ConfigError("Two type descriptions for one config element")
        meta["type"] = rest
    elif ctrl == "@":
        if not "dep" in meta:
            meta["dep"] = {}
        [var, val] = rest.split("=", maxsplit=1)
        meta["dep"][var] = val
    elif ctrl == "?":
        if not "ex" in meta:
            meta["ex"] = []
        meta["ex"] += [rest.split("=", maxsplit=1)[1]]
