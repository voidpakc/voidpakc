import os

def _read_ldconfig():
    """Parse the dynamic library loader config.

    The directory names are read from /etc/ld.so.conf, and the files
    in /etc/ld.so.conf.d if there is an include line in /etc/ld.so.conf.

    Include lines for other directories are not currently supported.

    The dirname=TYPE format is not supported, as that has not been
    needed since about 1995, so is unlikely to make an appearance.

    A list of directory names is returned.

    TODO: Musl support (https://musl.libc.org/doc/1.1.24/manual.html)
    Musl uses /etc/ld-musl-$ARCH.path, which is separated by either
    newlines or colons, defaulting to "/lib:/usr/local/lib:/usr/lib".

    References:
        ldconfig(8) (http://man7.org/linux/man-pages/man8/ldconfig.8.html)

    """

    include = False

    def read_file(path):
        dirs = []
        try:
            with open(path) as f:
                for l in f:
                    l = l.split("#")[0].split("=")[0].strip()

                    if l.startswith("include"):
                        if l != "include /etc/ld.so.conf.d/*.conf" and \
                           l != "include ld.so.conf.d/*.conf":
                            raise RuntimeError("Don't know how to handle "
                                               f"line '{l}' in {path}")

                        nonlocal include
                        include = True

                        continue

                    if l:
                        dirs += [l]

        except FileNotFoundError:
            pass
        return dirs

    dirs = []

    conf_path = "/etc/ld.so.conf"
    dirs += read_file(conf_path)

    conf_dir = f"{conf_path}.d"
    if include and os.path.isdir(conf_dir):
        # The search order matters, so we need to sort the filenames.
        for f in sorted(os.listdir(conf_dir)):
            if f.endswith(".conf"):
                dirs += read_file(f"{conf_dir}/{f}")

    return dirs

def _lib_index():
    """Create a dynamic library index.

    Searching is done in a way that should produce similar results to
    an unpatched glibc dynamic linker in most cases.

    LD_LIBRARY_PATH is searched first, then the directories listed in
    /etc/ld.so.conf{,.d} and finally "trusted directories" such as
    /usr/lib and /lib.

    Returned is a list of (directory, file list) tuples.

    """

    dir_list = [
        *os.getenv("LD_LIBRARY_PATH", default="").split(":"),
        *_read_ldconfig(),
        "/usr/lib64", "/lib64", "/usr/lib", "/lib"
    ]

    libs = []

    # On some systems, /lib and /usr/lib are symlinked together, so we
    # don't want to search them twice.
    already_searched = set()

    for d in dir_list:
        if not d.strip() or not os.path.isdir(d):
            continue

        # Resolve any symlinks in the directory path
        rp = os.path.realpath(d)
        if rp in already_searched:
            continue
        already_searched.add(rp)

        # Order shouldn't really matter, but it's nicer having a list
        # that is the same every execution.
        libs += [(d, sorted(os.listdir(d)))]

    return libs

lib_list = _lib_index()

def find_lib(lib):
    for d, libs in lib_list:
        if lib in libs:
            return d
