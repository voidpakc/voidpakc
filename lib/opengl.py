import os

import common
from lib_search import lib_list, find_lib

def find_opengl(extra_libs = {}):
    gl_bind = []

    gldir = find_lib("libGL.so")

    if not gldir:
        raise RuntimeError("Could not find OpenGL libraries (libGL.so)")

    gl_vendor = ""
    mesa_libs = ["GLX", "EGL", "GL", "GLESv1_CM", "GLESv2"]

    # Check for glvnd:
    if os.path.isfile(f"{gldir}/libGLdispatch.so.0"):
        print("glvnd detected")

        # Assume Mesa:
        gl_vendor = "_mesa"

        gldir = find_lib(f"libGLX{gl_vendor}.so")

    mesa_libs = [f"{l}{gl_vendor}" for l in mesa_libs] + ["gbm", "glapi", "MesaOpenCL"]

    # Files in the directory that the GL libraries are in
    mesa_ls = [libs for d, libs in lib_list if d == gldir][0]

    # If the GL library directory has no other files, we can bind in
    # the whole directory rather than doing it one file at a time.
    bind_dir = True
    for lib in mesa_ls:
        if not lib.startswith("lib"):
            continue

        so_i = lib.find(".so")
        if so_i == -1:
            continue

        if not lib[3:so_i] in mesa_libs:
            bind_dir = False
            break

    if bind_dir:
        print(f"Binding directory {gldir}")
        gl_bind += ["--ro-bind", gldir, "/tmp/gl"]
    else:
        for l in mesa_libs:
            l = f"lib{l}.so"
            for lib in mesa_ls:
                if lib.startswith(l):
                    gl_bind += ["--ro-bind", f"{gldir}/{lib}", f"/tmp/gl/{lib}"]


    dri_dir = common.first_existing_of(
        [f"{gldir}/{d}" for d in ["xorg/modules/dri", "dri", "lib/dri"]],
        dir=True,
        err=RuntimeError("Could not find the Mesa drivers directory"
                         " (e.g. /usr/lib/dri)")
    )

    gl_bind += ["--ro-bind", dri_dir, "/tmp/dri"]

    already_bound = set()

    # These libraries are not in the runtime image, but Mesa might
    # depend on them, so we need to try and bind them into the container.
    bind_libs = {"libwayland-client", "libwayland-server", "libsensors", "libelf"}
    bind_libs.update(extra_libs)

    for lib_dir, llist in lib_list:
        for lib in llist:
            if lib.split(".")[0] in bind_libs \
               or ( lib.startswith("libLLVM") \
                    and lib.find(".so") != -1):
                if lib in already_bound:
                    continue
                already_bound.add(lib)
                gl_bind += ["--ro-bind", f"{lib_dir}/{lib}", f"/tmp/libs/{lib}"]

    return gl_bind
