import collections
import os
import shlex

import iniparser

def _prf_dict():
    return collections.defaultdict(list)

PRFDIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

PRF = f"{PRFDIR}/profiles"

prf = _prf_dict()

def _parse_profiles(filename):
    prf = _prf_dict()

    with open(filename) as f:
        current_sections = []

        for line in f:

            new_section = iniparser.parse_section(line)
            if new_section:
                sect = new_section.split("+", maxsplit=1)

                games = sect[0].split(",") if len(sect[0]) else [""]
                sf = f"+{sect[1]}" if len(sect) == 2 else ""

                if sf in {"+", ","} or (games == [""] and not sf):
                    # TODO: Better error message
                    raise RuntimeError("Invalid profile section")

                current_sections = [x + sf for x in games]

                continue

            line = shlex.split(line, comments=True)

            if not line:
                continue

            for s in current_sections:
                prf[s] += [line]

    return prf

def read_profiles():
    global prf
    try:
        prf = _parse_profiles(PRF)
    except FileNotFoundError:
        prf = _prf_dict()

def _get_prof(name):
    return prf[name] if name in prf else []

def get_profile(game, flags):
    pl = _get_prof(f"common")
    pl += _get_prof(game)

    for f in flags:
        pl += _get_prof(f"+{f}")
        pl += _get_prof(f"{game}+{f}")

    return pl

if __name__ == "__main__":
    read_profiles()
    print(get_profile("stk", ["hud"]))
