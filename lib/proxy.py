import common

def proxychains_conf():
    """Setup a proxy config file for proxychains-ng.

    Although many programs have support for environment variables such
    as SOCKS_PROXY, for a consistent experience it is eaiser to use a
    preloaded proxy library such as proxychains. Another reason for
    using a separate library is that many clients do not support
    proxied DNS.

    If the configuration file lists a proxy, the config file for use
    by proxychains is returned in string form, else None is returned.

    """

    proxy = common.get_config("Container", "socks proxy")

    if not proxy:
        return None

    errormsg = "Incorrectly formated proxy URL (%s)"

    sp = proxy.split("/")
    if len(sp) == 3: # e.g. socks5://127.0.0.1:1234
        ptype = sp[0].rstrip(":")
        purl = sp[2]
    elif len(sp) == 1: # e.g. 127.0.0.1:1234
        ptype = "socks5"
        purl = proxy
    else:
        raise ConfigError(errormsg % "incorrect number of '/' characters")

    try:
        [url, port] = purl.split(":")
    except ValueError:
        raise ConfigError(errormsg % "no port number")

    try:
        portnum = int(port)
        if portnum < 0 or portnum > 65535:
            raise ValueError()
    except ValueError:
        raise ConfigError(errormsg % "port number must be a 16-bit integer")

    config = "\n".join(["strict_chain", "quiet_mode", "proxy_dns",
                        "tcp_read_time_out 15000", "tcp_connect_time_out 8000",
                        "localnet 127.0.0.0/255.0.0.0",

                        "[ProxyList]",
                        f"{ptype} {url} {port}",
                        ""
    ])

    return config
