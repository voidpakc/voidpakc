import shlex

class RundatError(RuntimeError):
    pass

def num_arg_error(cmd, num, got):
    s = "s" if num > 1 else ""
    raise RundatError(f"{cmd} command takes {num} argument{s}; got {got}")

def num_arg_check(cmd, num, got):
    if num != got:
        num_arg_error(cmd, num, got)

def parse(filename, print_run=False):
    single = ["game", "runtime", "type", "wd"]
    combine = ["clean", "cmd", "name", "use"]
    multi = ["bind", "bind-try", "create", "dir", "env", "symlink", "template", "tmpfs"]

    run = {}

    with open(filename) as f:
        for l in f:
            a = shlex.split(l, comments=True)
            if not len(a):
                continue
            cmd = a[0]
            a = a[1:]
            if cmd in single:
                if cmd in run:
                    raise RundatError(f"Duplicate rundat entry '{cmd}'")
                run[cmd] = a
            elif cmd in combine:
                if cmd in run:
                    run[cmd] += a
                else:
                    run[cmd] = a
            elif cmd in multi:
                if cmd not in run:
                    run[cmd] = []
                run[cmd] += [a]
            else:
                raise RundatError(f"Unknown rundat command '{cmd}'")

    if print_run:
        print(run)

    # This allows us to skip on existence checks later
    for l in [single, combine, multi]:
        for x in l:
            if not x in run:
                run[x] = []

    return run
