import os

import common

def find_rt(rt_type):
    name = "devel" if rt_type == "devel" else "rt"

    runtime_dir = common.get_config("Paths", "images") + "/" + name

    if not os.path.isdir(runtime_dir):
        raise RuntimeError(f"Runtime image directory {runtime_dir} does not exist")

    image = None
    for f in os.listdir(runtime_dir):
        if not f.endswith(".img"):
            continue

        # The runtimes are stored using an ISO date (e.g. 2020-01-23)
        # so we can just do a normal string compare:
        if image and f < image:
            continue

        image = f

    if not image:
        raise RuntimeError("Could not find a runtime image in " + runtime_dir)

    return runtime_dir + "/" + image

def get_rt_name(path):
    return os.path.splitext(os.path.basename(path))[0]

class Runtime:
    def __init__(self, rt_type):
        if rt_type not in ["rt", "devel"]:
            raise RuntimeError("Unknown runtime type " + rt_type)

        self.image = find_rt(rt_type)
        self.name = get_rt_name(self.image)
        self.mnt = common.get_mnt_dir(rt_type, self.name)

        print("Using runtime", self.name)

    def mount(self, squashfs):
        squashfs.mount(self.image, self.mnt)

    def bind(self):
        return ["--ro-bind", self.mnt, "/"]
