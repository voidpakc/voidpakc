#!/usr/bin/env python3

import argparse
import hashlib
import os
import plistlib
import subprocess
import time
import urllib.request as request

import common
from common import bold_print as bprint

def sha256sum(path):
    """Return a sha256 hex digest of a file."""

    BLOCKSIZE = 32768
    sha = hashlib.sha256()

    with open(path, "rb") as f:
        buf = f.read(BLOCKSIZE)
        while buf:
            sha.update(buf)
            buf = f.read(BLOCKSIZE)

    return sha.hexdigest()

def verify(sha256, sigfile, pubkey):
    d = subprocess.run(["openssl", "rsautl", "-verify", "-in", sigfile,
                        "-pubin", "-inkey", "/dev/stdin"],
                       input=pubkey, capture_output=True).stdout[15:]

    return d.hex() == sha256

def verify_xbps(sha256, sigfile):
    KEYDIR = "/var/db/xbps/keys/"

    keys = os.listdir(KEYDIR)

    keystr = []

    for key in keys:
        with open(KEYDIR + key, "rb") as f:
            plist = plistlib.load(f)

            keystr += [plist["public-key"]]

    for key in keystr:
        if verify(sha256, sigfile, key):
            return True

    return False

def check_xbps(xbps_file, expected_sha256):
    sha256 = sha256sum(xbps_file)

    if sha256 != expected_sha256:
        bprint("SHA256SUM mismatch!")
        return False

    if not verify_xbps(sha256, xbps_file + ".sig"):
        bprint("RSA verification failure!")
        return False

    return True

def get_pkg_info(pkg, flags=[]):
    d = subprocess.run(["xbps-query", *flags, "-Rp",
                        "repository,pkgver,architecture,filename-sha256",
                        pkg], capture_output=True,
                       encoding="utf-8", check=True).stdout.split("\n")

    name = f"{d[1]}.{d[2]}.xbps"
    url = f"{d[0]}/{name}"
    sha256 = d[3]

    return (name, url, sha256)

if __name__ == "__main__":
    bprint("\nDownloading source files\n")

    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument("--dl", nargs=3, action="append", default=[])
    parser.add_argument("--pkg", action="append", default=[])

    args = parser.parse_args()

    shafail = False

    for url, sha, dlname in args.dl:
        fpath = f"/tmp/dlf/{sha}"

        if os.path.exists(fpath):
            bprint(f"{url} already downloaded")

            info = f"{fpath}.info"
            if os.path.exists(info):
                with open(info) as f:
                    info_contents = f.read()
                    info_url = [*filter(len, info_contents.split("\n"))][-1]

                if info_url != url:
                    bprint(f"\nWarning: This URL:\n    {url}")
                    bprint("is marked as having the same content as this one:")
                    bprint(f"    {info_url}\n")
                    bprint("If this is an error, change the SHA256SUM in the info file")
                    bprint(f"(currently {sha})")
                    bprint("to be something different (e.g. delete a few characters)\n")

                    time.sleep(15)

        else:
            fpath = fpath + ".tmp"

            bprint(f"Downloading {url} to {fpath}")

            with open(f"{fpath}.info", "w") as f:
                f.write(url + "\n")

            ret = subprocess.run([
                "wget", "--progress=dot:mega", "--xattr", "--continue",
                "--hsts-file=/tmp/dlf/wget-hsts", "--timeout=10",
                "-O", fpath,
                "--", url
            ]).returncode

            if ret:
                bprint(f"\nFailed to download {url}")
                exit(1)

        bprint("Verifying... ", end="", flush=True)

        actual_sha = sha256sum(fpath)

        os.rename(fpath, f"/tmp/dlf/{actual_sha}")
        try:
            os.rename(f"{fpath}.info", f"/tmp/dlf/{actual_sha}.info")
        except FileNotFoundError:
            pass

        if actual_sha == sha:
            bprint("done.")
        else:
            bprint("SHA256SUM mismatch!")
            bprint(f"Expected: {sha}")
            bprint(f"Got:      {actual_sha}")

            shafail = True

    print()

    if shafail:
        raise RuntimeError("SHA256SUM mismatch")

    for pkg in args.pkg:
        # TODO: Catch exceptions from subprocess.run
        name, url, sha256 = get_pkg_info(pkg)

        if not os.path.isdir("/tmp/dlf/vpkg"):
            os.mkdir("/tmp/dlf/vpkg")

        file_ = "/tmp/dlf/vpkg/" + name

        bprint(f"Downloading {url} to {file_}")

        for s in ("", ".sig"):
            if os.path.isfile(file_ + s):
                continue

            ret = subprocess.run([
                "wget", "--progress=dot:mega",
                "--hsts-file=/tmp/dlf/wget-hsts",
                "--timeout=10", "-O", file_ + s,
                "--", url + s
            ]).returncode

            if ret:
                # TODO: Update repo and retry
                bprint(f"\nFailed to download {url+s}")
                exit(1)

        bprint("Verifying... ", end="", flush=True)

        if check_xbps(file_, sha256):
            bprint("done.")
        else:
            os.remove(file_)
            os.remove(file_ + s)

            raise RuntimeError("Package verification failure")

            # TODO: Retry

        PKG_EXTRACT = "/tmp/src/pkg"

        common.mkdir_parents(PKG_EXTRACT)

        subprocess.run(["tar", "-C", PKG_EXTRACT,
                        "--strip-components=2",
                        "-xf", file_], check=True)

    print()
