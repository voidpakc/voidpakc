import os

import common

class Squashfs:
    def __init__(self):
        self.comm = common.find_squashfuse()

    def mount(self, src, dest):
        print("Mounting", src)

        if not os.path.isdir(dest):
            print("Creating", dest)
            common.mkdir_parents(dest)

        if len(os.listdir(dest)):
            print("Already mounted")
            return

        # squashfuse doesn't recognise '--', so we need to use
        # realpath, which makes sure paths will start with '/'
        [src, dest] = map(os.path.realpath, [src, dest])

        common.run([self.comm, src, dest])

    def unmount(self, path):
        # TODO: Some distributions require fusermount -u
        common.run(["umount", "-v", "--", path])
