#!/usr/bin/env python3

import argparse
import collections
import glob
import os
import shutil
import sys

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/lib")

import common
import opengl
import profiles
import proxy
import rundat
from rundat import RundatError
from runtime import Runtime
from squashfs import Squashfs

def mount(mounter, game, catg, ro=True):
    if ro:
        bind = "--ro-bind"
    else:
        bind = "--bind"

    dest = f"/tmp/{catg}"

    mi = common.MountDirs(catg, game)

    if os.path.isdir(mi.unpacked):
        return bind, mi.unpacked, dest

    if ro and os.path.isfile(mi.image):
        md = mi.mnt

        common.mkdir_parents(md)

        mounter.mount(mi.image, md)

        return bind, md, dest

    raise RuntimeError(f"Could not find the game files files for {game}\n" \
                       f"Tried {mi.unpacked} and {mi.image}")

def find_data_rd(compat, game=""):
    supports = collections.defaultdict(list)
    names = {}

    rd = common.get_config("Paths", "images") + "/run/data/"
    for f in os.listdir(rd):
        if not f.endswith(".rundat"):
            continue

        rd_info = rundat.parse(rd + f)
        name = rd_info["name"]
        names[f] = " ".join(name) if name else f[:-len(".rundat")]

        type_ = rd_info["type"]
        if type_[:1] == ["data"]:
            for r_game in type_[1:]:
                supports[r_game] += [f]

    # Potential data files that can be used
    pot = set()

    # For each data type the game supports
    for c in compat:
        if c in supports:
            # Add any games of this type
            pot |= set(supports[c])

    if pot == set():
        print(f"Could not find any data files for {game}")
        print("You could try installing one of:")

        for f in glob.glob("scripts/**/*.rundat", recursive=True):
            type_ = rundat.parse(f)["type"]
            if type_[:1] == ["data"] and \
               set(type_[1:]).intersection(compat):
                print("-", os.path.basename(os.path.dirname(f)))

        sys.exit(1)

    if len(pot) == 1:
        return rd + pot.pop()

    pot_list = sorted(pot)

    selected = common.select("Game data to use:", [names[w] for w in pot_list])

    return rd + pot_list[selected]

def find_data(compat, game="", user_sel=None):
    rd = common.find_rundat(user_sel, data=True) if user_sel else None

    if not rd:
        rd = find_data_rd(compat, game)

    game = ""
    game_bind = []

    run = rundat.parse(rd)

    rundat.num_arg_check("game", 1, len(run["game"]))
    game = run["game"][0]

    for b in run["bind"]:
        rundat.num_arg_check("game", 2, len(b))
        game_bind += [b]

    return game, game_bind

def bind_from_home(name, dest=None):
    if dest == None:
        dest = name
    home = os.getenv("HOME")
    if home:
        file_ = f"{home}/{name}"
        if os.path.exists(file_):
            return ["--ro-bind", file_, f"/tmp/home/{dest}"]

    return []

def main(argv):
    common.banner()

    f_env = ["--setenv", "LD_LIBRARY_PATH", "/tmp/gamelib:/tmp/gl2:/tmp/gl:/tmp/libs",
             "--setenv", "LIBGL_DRIVERS_PATH", "/tmp/dri"]

    # Parse command line arguments
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument("--devel", dest="rt_type", action="store_const", default="rt", const="devel",
                        help="use a the development runtime image")
    parser.add_argument("--shell", action="store_true", help="start a shell in the continaer")
    parser.add_argument("--game", dest="data", help="data files to use")
    parser.add_argument("--tmp-conf", action="store_true", help="use a temporary config directory")

    parser.add_argument("args", metavar="[NAME=VALUE]... +profile... GAME [ARG]...",
                        nargs=argparse.REMAINDER)

    args = parser.parse_args()

    profile_list = []

    n = 0
    for a in args.args:
        if "=" in a:
            f_env += ["--setenv"] + a.split("=", 1)
        elif a[:1] == "+":
            profile_list += [a[1:]]
        elif n and a[:1] == "-":
            parser.error("options after environment variables")
        else:
            break
        n += 1

    if len(args.args) == n:
        parser.error("no game specified")

    # Search for the game
    game = args.args[n].rstrip("/")

    f_args = args.args[n+1:]

    runf = common.find_rundat(game)
    if not runf:
        parser.error(f"could not find game {game}")

    runf_name = os.path.basename(runf).split(".")[0]

    run = rundat.parse(runf, print_run=True)

    # Read configuration from profiles
    use_disallow = []
    bind_libs = set()

    profiles.read_profiles()

    prf = profiles.get_profile(runf_name, profile_list)

    f_prf_env = []

    for cmd, *arg in prf:
        if cmd == "env":
            rundat.num_arg_check("env", 2, len(arg))

            f_prf_env += ["--setenv", *arg]
        elif cmd == "disable":
            use_disallow += arg
        elif cmd == "bind":
            rundat.num_arg_check("bind", 3, len(arg))

            if arg[0] not in ["ro", "rw"]:
                raise RuntimeError("First argument after 'bind' must be "
                                   "rw or ro")

            bind = "--bind" if arg[0] == "rw" else "--ro-bind"

            f_prf_env += [bind, *arg[1:]]
        elif cmd == "lib":
            bind_libs.update(set(arg))
        else:
            raise RuntimeError(f"Unknown profile command {cmd}")

    f_env = f_prf_env + f_env

    env_pass = common.get_config("Container", "env passthrough").split()

    # Define some variables that will be passed as command-line
    # arguments to bwrap.
    f_unshare = ["--unshare-user-try", "--unshare-pid", "--unshare-uts",
                 "--unshare-cgroup-try"]
    f_sys_bind = []
    f_rundat = []
    f_wrappers = []

    # List of files that need to be kept open during execution
    open_file_list = []

    # Mapping from file descriptors to content.
    # This is used to print out herestrings when displaying the bwrap
    # command to be executed.
    fd_map = {}

    bind_devshm = False
    bind_sys = False

    # Mapping from mount types to a (foo, bar, baz) tuple
    mntdir = {}

    sq = Squashfs()

    rt = Runtime(args.rt_type)
    rt.mount(sq)

    mntdir["rt"] = [*rt.bind(), "--tmpfs", "/tmp"]

    f_sys_bind += ["--proc", "/proc",
                   "--dev", "/dev",

                   "--dir", "/tmp/home",
                   "--dir", "/tmp/libs",
    ]

    def add_data_bind(text, target):
        nonlocal open_file_list
        nonlocal f_sys_bind
        file_ = common.tempfile_write(text)
        open_file_list += [file_]
        fd = file_.fileno()
        f_sys_bind += ["--bind-data", str(fd), target]
        fd_map[fd] = text


    if not run["game"] or not run["game"][0]:
        raise RundatError("No game name specified in rundat")

    if len(run["game"]) != 1:
        raise RuntimeError("'game' rundat field should contain one word")

    game_name = run["game"][0]

    if not game_name.replace("-", "_").isidentifier() or \
       not game_name.isascii():
        raise RundatError("Game names should only contain alphanumeric characters and '-'")

    mntdir["bin"] = mount(sq, game_name, "bin")

    if args.tmp_conf:
        mntdir["rt"] += ["--tmpfs", "/tmp/conf"]
    else:
        conf_dir = common.get_config("Paths", "images") + "/conf/" + game_name
        common.mkdir_parents(conf_dir)
        mntdir["conf"] = ("--bind", conf_dir, "/tmp/conf")

    def has_use(use):
        if use in run["use"]:
            if use not in use_disallow:
                return True
        return False

    if len(run["type"]):
           r_tp = run["type"]
           g_type = r_tp[0]
           g_compat = r_tp[1:]

           if g_type != "game":
               raise RuntimeError("Data files and libraries cannot be run!")

           data_name, data_bind = find_data(g_compat, game, args.data)

           mntdir["data"] = mount(sq, data_name, "data")

           for src, dest in data_bind:
               run["bind"] += [[f"data/{src}", f"/tmp/data/{dest}"]]

    f_mnt = []
    # We are guaranteed to get the keys in insertion order:
    # https://mail.python.org/pipermail/python-dev/2017-December/151283.html
    for type_ in mntdir:
        f_mnt += mntdir[type_]

    def bind_info(path):
        ptype = path.split("/", maxsplit=1)[0]

        if not ptype in mntdir:
            raise RundatError(f"{srctype} not mounted")

        return mntdir[ptype]

    def from_mntrel(path, rw_check=True):
        relpath = path.split("/", maxsplit=1)[1]

        bind, basedir = bind_info(path)[:2]

        if rw_check and bind == "--bind" and relpath:
            raise RundatError("Not using relative path from writable location "
                              f"{ptype} because of security risk")

        return common.join_path_checked(basedir, relpath)

    if has_use("gl"):
        print("Enabling OpenGL")

        gl_bind = opengl.find_opengl(bind_libs)

        gl4es = common.get_config("OpenGL", "gl4es path")
        if gl4es:
            gl_bind += ["--ro-bind", gl4es, "/tmp/gl2"]

        gl_bind += ["--dev-bind", "/dev/dri", "/dev/dri"]

        f_sys_bind += gl_bind
        bind_sys = True

    def allow_x():
        e_display = os.getenv("DISPLAY")

        if not e_display:
            print("DISPLAY not set!")
            return []

        if e_display and e_display.rstrip("0123456789") == ":":
            display = e_display[1:]
            print(f"Giving access to X display {display}")
            path = f"/tmp/.X11-unix/X{display}"
        else:
            print("Giving access to all X displays")
            path = "/tmp/.X11-unix"

        ret = ["--ro-bind", path, path]

        ret += bind_from_home(".Xauthority")

        ret += ["--setenv", "DISPLAY", e_display]

        return ret

    def allow_wl():
        e_display = os.getenv("WAYLAND_DISPLAY", default="wayland-0")
        e_runtime_dir = os.getenv("XDG_RUNTIME_DIR")

        if not e_runtime_dir:
            return []

        socket = e_runtime_dir + "/" + e_display

        if not os.path.exists(socket):
            return []

        print(f"Giving access to Wayland display {e_display}")

        return ["--ro-bind", socket, "/tmp/run/wayland-0",
                "--setenv", "WAYLAND_DISPLAY", "wayland-0"]

    if has_use("x") or has_use("gl"):
        f_sys_bind += allow_x()
        f_sys_bind += allow_wl()

    if has_use("net"):
        print("Giving network access to the container")

        proxycfg = proxy.proxychains_conf()
        if proxycfg:
            print("Setting up proxy")
            f_wrappers += ["proxychains4"]

            add_data_bind(proxycfg, "/etc/proxychains.conf")

        f_sys_bind += ["--ro-bind", "/etc/resolv.conf", "/etc/resolv.conf"]
    else:
        f_unshare += ["--unshare-net"]

    if has_use("ipc"):
        print("Not isolating IPC in container")
    else:
        f_unshare += ["--unshare-ipc"]

    if has_use("passwd"):
        user = os.getenv("USER")
        uid = os.getenv("UID")
        if not user:
            user = "rms"
        if not uid:
            uid = 1000

        add_data_bind(f"{user}:x:{uid}:{uid}::/tmp/home:/bin/bash",
                      "/etc/passwd")

    if has_use("audio"):
        # TODO: Pulseaudio
        print("Audio enabled")
        f_sys_bind += ["--dev-bind", "/dev/snd", "/dev/snd"]

        f_sys_bind += bind_from_home(".asoundrc")

        # TODO: Improve this
        if True or os.path.exists("/dev/shm/jack-1000-socket") or \
           os.path.exists("/dev/shm/jack_default_1000_0"):
            print("Detected JACK running")
            bind_devshm = True
            f_sys_bind += ["--ro-bind-try", "/usr/lib/libjack.so.0", "/usr/lib/libjack.so.0",
                           "--ro-bind-try", "/usr/lib/libjack.so.0.999.0", "/usr/lib/libjack.so.0"]
            # TODO: Option for netjack (through named socket)
            # TODO: alsa->jack bridge
            # TODO: Bind in /lib/libjack.so*

    if bind_devshm:
        f_sys_bind += ["--dev-bind", "/dev/shm", "/dev/shm"]

    f_sys_bind += ["--dev-bind", "/dev/input", "/dev/input"]

    if bind_sys:
        f_sys_bind += ["--ro-bind", "/sys", "/sys"]

    for d in run["dir"]:
        rundat.num_arg_check("dir", 1, len(d))

        dest = common.join_path_checked(mntdir["conf"][1], d[0])

        if not os.path.exists(dest):
            print(f"Creating directory {dest}")
            os.mkdir(dest)
        elif not os.path.isdir(dest):
            raise RundatError(f"{dest} exists, but is not a directory")

    for c in run["create"]:
        rundat.num_arg_check("create", 1, len(c))

        dest = common.join_path_checked(mntdir["conf"][1], c[0])

        if not os.path.exists(dest):
            print(f"Creating {dest}")
            with open(dest, "w"):
                pass
        elif not os.path.isfile(dest):
            raise RundatError(f"{dest} exists, but is not a regular file")

    for t in run["template"]:
        rundat.num_arg_check("template", 2, len(t))

        src, dest = t

        src = from_mntrel(src)
        dest = common.join_path_checked(mntdir["conf"][1], dest)

        if not os.path.exists(dest):
            print(f"Copying from {src} to {dest}")
            shutil.copyfile(src, dest, follow_symlinks=False)

    for b in run["bind"]:
        rundat.num_arg_check("bind", 2, len(b))

        src, dest = b

        f_rundat += [bind_info(src)[0], from_mntrel(src), dest]

    for b in run["bind-try"]:
        rundat.num_arg_check("bind-try", 2, len(b))

        src, dest = b

        f_rundat += [bind_info(src)[0] + "-try", from_mntrel(src), dest]

    for t in run["tmpfs"]:
        rundat.num_arg_check("tmpfs", 1, len(t))
        f_rundat += ["--tmpfs"] + t

    if not len([x for x in run["symlink"] if x[1] == "/tmp/gamelib"]):
        run["symlink"] += [["/tmp/bin/libs", "/tmp/gamelib"]]

    for s in run["symlink"]:
        rundat.num_arg_check("symlink", 2, len(s))
        f_rundat += ["--symlink"] + s

    for e in run["env"]:
        rundat.num_arg_check("env", 2, len(e))
        f_rundat += ["--setenv"] + e

    if run["wd"]:
        rundat.num_arg_check("wd", 1, len(run["wd"]))
        f_rundat += ["--chdir"] + run["wd"]

    f_cmd = run["cmd"]
    if args.shell:
        cmdname = " ".join(f_cmd + f_args)
        print(f"Opening a shell, not running '{cmdname}'")
        f_cmd = ["bash"]
        f_args = []
    if not f_cmd:
        raise RundatError("No command to run specified in rundat")

    # TODO: Game start / stop hooks

    ret = common.run([common.find_bwrap(), *common.bwrap_common_args(),
                      *f_unshare, *f_mnt, *f_sys_bind, *f_rundat, *f_env,
                      "--", *f_wrappers, *f_cmd, *f_args
    ], fd_map, env_pass=env_pass)

    # TODO: Remove empty files if 'clean conf' specified

    return ret

exit(main(sys.argv))
