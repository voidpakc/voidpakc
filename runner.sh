#!/bin/bash

set -e

usage() {
    printf "USAGE: %s [VAR=VAL]... PROGRAM [ARGS]...\n" "$0"
    printf "\nInfluential environment variables: \n"
    printf "    DEVRT=1     Use a devel runtime\n"
    printf "    DEVSHELL=1  Launch a shell inside the container\n"
}

if ! [ "$1" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    usage
    exit 1
fi

cd "$(dirname "$(realpath -- "$0")")"

if ! [ -e config ]; then
    printf "Could not find config file\n"
    exit 1
fi

. ./config

if ! [ "$SQUASHFUSE" ]; then
    SQUASHFUSE="$IMAGE_DIR"/static/squashfuse
fi

verbose() {
    ( IFS=' '; printf "%s\n" "$*" )
    "$@"
}

ENV=( )

while printf "%s" "$1" | grep -q =
do
    ENV+=( --setenv "$(printf "%s" "$1" | cut -d= -f1)" "$(printf "%s" "$1" | cut -d= -f2-)" )
    shift 1
done

RF="$IMAGE_DIR"/run/"$1".rundat

if ! [ -e "$RF" ]; then
    if [ -e "${1%.rundat}.rundat" ]; then
        RF="$1"
    else
        printf "Could not find game \"%s\". " "$1"
        printf "Have you compiled it yet?\n\n"
        printf "I tried looking in %s .\n\n" "$IMAGE_DIR"/run
        usage
        exit 1
    fi
fi

CT_LOCALE="$(printf "%s" "$LOCALES" | cut -d" " -f1)"

FLAGS=( --unshare-user-try --unshare-pid --unshare-uts --unshare-cgroup-try
        --setenv DISPLAY "$DISPLAY" --setenv LANG "$CT_LOCALE" --setenv LC_COLLATE "C" )

getflags() {
    grep "^$1 " "$RF" | cut -d" " -f2-
}

GAME="$(getflags game)"

USE=( $(getflags use) )

AFTER_RT=( --tmpfs /tmp --dir /tmp/home --setenv HOME /tmp/home --proc /proc --dev /dev --setenv LD_LIBRARY_PATH /tmp/gamelib )

USE_X=0
USE_WL=0
USE_NET=0
USE_IPC=0
USE_AUDIO=0
USE_DEVSHM=0
for flag in "${USE[@]}"
do
    case "$flag" in
        gl)
            if ! [ "$GL_LIBS" ]; then
                # TODO: Handle glvnd in /usr/local/lib
                if [ -e /usr/local/lib/libGL.so ]; then
                    GLPATH=/usr/local/lib
                    GLVND=
                elif [ -e /usr/local/lib/libGLX_mesa.so ]; then
                    GLPATH=/usr/local/lib
                    GLVND=1
                elif [ -e /usr/lib/libGLX_mesa.so ]; then
                    GLPATH=/usr/lib
                    GLVND=1
                elif [ -e /usr/lib/libGL.so ]; then
                    GLPATH=/usr/lib
                    GLVND=
                else
                    printf "Could not find Mesa. Check your config file is correct.\n"
                    exit 1
                fi
                if [ "$GLVND" = 1 ]; then
                    for lib in /usr/lib/lib{EGL,GL,GLESv2,GLX,GLdispatch,OpenGL}.so.* "$GLPATH"/lib{EGL_mesa,GLX_indirect,GLX_mesa,gbm,glapi}.so.*
                    do
                        AFTER_RT+=( --ro-bind-try "$lib" "/tmp/gl/$(basename "$lib")" )
                    done
                    # TODO: Do we need glvnd cfg files?
                else
                    for lib in "$GLPATH"/lib{EGL,gbm,glapi,GLESv1_CM,GLESv2,GL}.so.*
                    do
                        AFTER_RT+=( --ro-bind-try "$lib" "/tmp/gl/$(basename "$lib")" )
                    done
                fi
                AFTER_RT+=( --ro-bind "$GLPATH"/dri /tmp/dri )
            else
                AFTER_RT+=( --ro-bind "$GL_LIBS" /tmp/gl --ro-bind "$GL_DRIVERS" /tmp/dri )
            fi
            if [ -e "$GL4ES" ]; then
                printf "Using gl4es\n"
                # TODO: Symlinks for other names (e.g. just libGL.so)
                AFTER_RT+=( --ro-bind "$GL4ES" /tmp/gl2/libGL.so.1 )
            fi
            AFTER_RT+=( --dev-bind /dev/dri /dev/dri --dir /tmp/libs --ro-bind /sys /sys
                        --setenv LD_LIBRARY_PATH /tmp/gamelib:/tmp/gl2:/tmp/gl:/tmp/libs --setenv LIBGL_DRIVERS_PATH /tmp/dri )
            # If Mesa was compiled to use LLVM, the user will already
            # have libLLVM, so we can use the one from the host...
            # Same for the other libraries.
            # TODO: Look in /usr/local/lib, /opt/lib etc.
            for lib in /usr/lib/libLLVM*.so /usr/lib/libsensors.so* /usr/lib/libwayland-{client,server}.so*
            do
                AFTER_RT+=( --ro-bind "$lib" "/tmp/libs/$(basename "$lib")" )
            done
            USE_X=1
            ;;
        x)
            USE_X=1
            ;;
        wayland)
            USE_WL=1
            ;;
        net)
            # TODO: Allow disabling net
            printf "Giving network access to the container\n"
            USE_NET=1
            ;;
        ipc)
            printf "Not isolating IPC in container\n"
            USE_IPC=1
            ;;
        audio)
            # TODO: Add a switch for disabling audio
            printf "Audio enabled\n"
            USE_AUDIO=1
            ;;
        passwd)
            COMPOSE_PASSWD=1
            ;;
        *)
            printf "Unsupported flag %s\n" "$i"
            ;;
    esac
done

if [ "$USE_X" = 1 ]; then
    if [ "$(printf "%s" "$DISPLAY" | sed 's/[0-9]*$//')" = ":" ]; then
        DDISP="$(printf "%s" "$DISPLAY" | grep -o '[0-9]*$')"
        printf "Giving access to X display %s\n" "$DDISP"
        AFTER_RT+=( --ro-bind /tmp/.X11-unix/X"$DDISP" /tmp/.X11-unix/X"$DDISP" )
    else
        printf "Giving access to all X displays\n"
        AFTER_RT+=( --ro-bind /tmp/.X11-unix /tmp/.X11-unix )
    fi
    if [ -e ~/.Xauthority ]; then
        AFTER_RT+=( --ro-bind "$(realpath ~/.Xauthority)" /tmp/home/.Xauthority )
    fi
fi

if [ "$USE_WL" = 1 ]; then
    if [ "$WAYLAND_DISPLAY" ]; then
        printf "Giving access to Wayland display %s\n" "$WAYLAND_DISPLAY"
        AFTER_RT+=( --bind "$XDG_RUNTIME_DIR"/"$WAYLAND_DISPLAY" /tmp/run/wayland-0 --setenv XDG_RUNTIME_DIR /tmp/run )
    fi
fi

if [ "$USE_NET" = 0 ]; then
    FLAGS+=( --unshare-net )
fi

if [ "$USE_IPC" = 0 ]; then
    FLAGS+=( --unshare-ipc )
fi

if [ "$USE_AUDIO" = 1 ]; then
    # TODO: Pulseaudio
    AFTER_RT+=( --dev-bind /dev/snd /dev/snd )
    if [ "$(shopt -s nullglob; printf "%s\n" /dev/shm/jack-"$UID"-*)" ]; then
        printf "Detected JACK running.\n"
        USE_DEVSHM=1
        # TODO: Add an option for using netjack (but can it be used
        # through a named socket?)
        # TODO: Get alsa->jack working

        # In case of incompatible versions
        for i in /usr/lib/libjack.so*
        do
            AFTER_RT+=( --ro-bind "$i" "/tmp/libs/$(basename "$i")" )
        done
    fi
fi

if [ "$USE_DEVSHM" = 1 ]; then
    AFTER_RT+=( --dev-bind /dev/shm /dev/shm )
fi

echo

MNT_DIR="$IMAGE_DIR"/mnt
mkdir -p "$MNT_DIR"

RT="$(getflags runtime | tr -d '\\/\n ')"
#BASERT="$(printf "%s" "$RT" | cut -d. -f-2)"
#POTRT=( "$IMAGE_DIR"/runtime/"$BASERT".*.img )
RTTYPE=rt
if [ "$DEVRT" = 1 ]; then
    RTTYPE=devel
fi
POTRT=( "$IMAGE_DIR"/"$RTTYPE"/*.img )
BESTRT="$(
IFS=$'\n'
echo "${POTRT[*]}" | sort -n -t. -k 3 | tail -n1
)"
if ! [ -f "$BESTRT" ]; then
    # The glob didn't match
    printf "ERROR: Could not find runtime compatabile with %s\n" "$RT"
    printf "The symlink trick may work...\n"
    exit 1
fi

if [ "$DEVRT" = 1 ]; then
    RTM="$MNT_DIR"/devel-"$(basename -- "$BESTRT" .img)"
else
    RTM="$MNT_DIR"/rt-"$(basename -- "$BESTRT" .img)"
fi
# TODO:
# Better dir "null check" - maybe
#     find -maxdepth 0 -empty
if ! [ -d "$RTM" ] || ! [ "$(ls "$RTM")" ]; then
    mkdir -p "$RTM"
    verbose "$SQUASHFUSE" "$BESTRT" "$RTM"
fi

FLAGS+=( --ro-bind "$RTM" /
         "${AFTER_RT[@]}" )

mntflag() {
    if [ "$RO" = 1 ]; then
        BIND=--ro-bind
    else
        BIND=--bind
    fi
    BASE="$IMAGE_DIR/$1"
    DEST="$(echo "$1" | tr -d -)"
    if [ -d "$BASE/$GAME" ]; then
        # TODO: Should we hardcode the first arg as dest?
        printf "%s\n%s\n%s\n" "$BIND" "$BASE/$GAME" "/tmp/$DEST"
        return
    elif [ "$RO" = 1 ] && [ -e "$BASE/$GAME.img" ]; then
        MD="$MNT_DIR"/"$1"-"$GAME"
        if ! [ -d "$MD" ] || ! [ "$(ls "$MD")" ]; then
            mkdir -p "$MD"
            verbose "$SQUASHFUSE" "$BASE/$GAME.img" "$MD" >&2
        fi
        printf "%s\n%s\n%s\n" "$BIND" "$MD" "/tmp/$DEST"
        return
    fi
    printf "Could not find the %s files for $GAME\n" "$*" >&2
    kill $$
}

TYPE=( $(getflags type) )

if [ "${TYPE[0]}" ] && ! [ "${TYPE[0]}" = game ]; then
    printf "Data files and libraries cannot be executed!\n"
    exit 1
fi

TYPE=( "${TYPE[@]:1}" )

IFS=$'\n'

find_data()
{
    set +e
    if ! [ "$IFS" = $'\n' ]; then
        printf "IFS must be \\n\n"
        kill $$
    fi
    DATA=
    POT=( )
    for t in "${TYPE[@]}"
    do
        # TODO: Stick data run files somewhere else
        POT+=( $(grep -l "type data.* $t\( \|$\)" "$IMAGE_DIR"/run/*.rundat) )
    done
    if [ "${#POT[@]}" = 0 ]; then
        printf "\n\nCould not find any data files for $GAME.\n"
        printf "You could try installing one of:\n"
        for t in "${TYPE[@]}"
        do
            grep -l "type data.* $t\( \|$\)" $(find scripts -name "*.rundat") | xargs dirname \
                | cut -d/ -f2- | sed 's/^/- /'
        done
        printf "\n"
        # TODO: Find a better way of terminating
        kill $$
    fi
    POT=( $(printf "%s\n" "${POT[@]}" |
                LC_COLLATE=C sort | LC_COLLATE=C uniq) )
    if [ "${#POT[@]}" = 1 ]; then
        DRD="${POT[0]}"
    else
        printf "\nData file:\n"
        CHOICES="$(printf "%s\n" "${POT[@]}" | xargs basename -s .rundat)"
        # TODO: Good name?
        if [ "$DATA_LOAD" ]; then
            DATA="$DATA_LOAD"
        else
            select DATA in $CHOICES
            do
                if [ "$DATA" ]; then
                    break
                fi
            done
        fi
        DO="$(printf "%s\n" "$CHOICES" | grep -Fnx "$DATA" | cut -d: -f1)"
        DRD="$(printf "%s\n" "${POT[@]}" | head -n "$DO" | tail -n1)"
    fi
    set -e
    DATA="$(grep game "$DRD" | cut -d" " -f2-)"
    local i
    for i in $(grep "^bind " "$DRD")
    do
        SRC="$(printf "%s" "$i" | cut -d" " -f2)"
        DEST="$(printf "%s" "$i" | cut -d" " -f3-)"
        DATBIND+=( "$SRC"$'\n'"$DEST" )
    done
}

declare -A MNTDIRS

for i in bin conf $([ "${TYPE[*]}" ] && echo data)
do
    case "$i" in
        bin)
            FL="$(RO=1 mntflag "$i")"
            FLAGS+=( $FL )
        ;;
        data)
            DATBIND=( )
            if [ "${TYPE[*]}" ]; then
                find_data
            fi
            FL="$(RO=1 GAME="$DATA" mntflag "$i")"
            FLAGS+=( $FL )
            for i in "${DATBIND[@]}"
            do
                SRC="$(printf "%s" "$i" | head -n1)"
                DEST="$(printf "%s" "$i" | tail -n1)"
                SB="$(printf "%s" "$FL" | head -n2 | tail -n1)"
                if [ "$(realpath --relative-base "$SB" -- "$SB/$SRC" | head -c1)" = / ]; then
                    printf "Bind %s failed.\n" "data $SRC $DEST"
                    continue
                fi
                FLAGS+=( --ro-bind "$SB/$SRC" /tmp/data/"$DEST" )
            done
        ;;
        conf)
            mkdir -p "$IMAGE_DIR/conf/$GAME"
            FL="$(mntflag "$i")"
            FLAGS+=( $FL )
        ;;
        *)
            printf "Unsupported mount item %s\n" "$i"
            continue
        ;;
    esac
    MNTDIRS["$i"]="$FL"
done

for i in $(getflags bind)
do
    SRCDIR="$(printf "%s" "$i" | cut -d/ -f1)"
    SRC="${MNTDIRS["$SRCDIR"]}"
    if [ "$SRC" ]; then
        BIND="$(printf "%s" "$SRC" | head -n1)"
        if [ "$(printf "%s" "$i" | cut -d" " -f3)" = try ]; then
            BIND="$BIND-try"
        fi
        DSRC="$(printf "%s" "$SRC" | head -n2 | tail -n1)"
        BSRC="$DSRC/$(printf "%s" "$i" | cut -d/ -f2- | cut -d" " -f1)"
        if [ "$(realpath --relative-base "$DSRC" -- "$BSRC" | head -c1)" = / ]; then
            printf "Bind %s failed\n" "$i"
            continue
        fi
        DEST="$(printf "%s" "$i" | cut -d" " -f2)"
        FLAGS+=( "$BIND" "$BSRC" "$DEST" )
    else
        printf "Bind %s failed.\n" "$i"
    fi
done

for i in $(getflags tmpfs)
do
    FLAGS+=( --tmpfs "$i" )
done

for i in $(getflags template)
do
    SRCDIR="$(printf "%s" "$i" | cut -d/ -f1)"
    SRC="${MNTDIRS["$SRCDIR"]}"
    if [ "$SRC" ]; then
        DSRC="$(printf "%s" "$SRC" | head -n2 | tail -n1)"
        BSRC="$DSRC/$(printf "%s" "$i" | cut -d/ -f2- | cut -d" " -f1)"
        if [ "$(realpath --relative-base "$DSRC" -- "$BSRC" | head -c1)" = / ]; then
            printf "Template %s failed\n" "$i"
            continue
        fi
        DDEST="$(printf "%s" "${MNTDIRS[conf]}" | head -n2 | tail -n1)"
        BDEST="$DDEST/$(printf "%s" "$i" | cut -d/ -f2- | cut -d" " -f2-)"
        if [ "$(realpath --relative-base "$DSRC" -- "$BSRC" | head -c1)" = / ]; then
            printf "Template %s failed\n" "$i"
            continue
        fi
        if ! [ -e "$BDEST" ]; then
            verbose cp -a -- "$BSRC" "$BDEST"
        fi
    else
        printf "Template %s failed.\n" "$i"
    fi
done

for i in $(getflags dir)
do
    DDEST="$(printf "%s" "${MNTDIRS[conf]}" | head -n2 | tail -n1)"
    BDEST="$DDEST/$(printf "%s" "$i")"
    if [ "$(realpath -m --relative-base "$DDEST" -- "$BDEST" | head -c1)" = / ]; then
        printf "Directory creation of %s failed\n" "$i"
        continue
    fi
    mkdir -pv -- "$BDEST"
done

for i in $(getflags create)
do
    DDEST="$(printf "%s" "${MNTDIRS[conf]}" | head -n2 | tail -n1)"
    BDEST="$DDEST/$(printf "%s" "$i")"
    if [ "$(realpath --relative-base "$DDEST" -- "$BDEST" | head -c1)" = / ]; then
        printf "Creating file %s failed\n" "$i"
        continue
    fi
    if ! [ -e "$BDEST" ]; then
        printf "Creating file %s\n" "$BDEST"
        touch -- "$BDEST"
    fi
done

for i in $(getflags symlink)
do
    SRC="$(printf "%s" "$i" | cut -d" " -f1)"
    DEST="$(printf "%s" "$i" | cut -d" " -f2-)"
    FLAGS+=( --symlink "$SRC" "$DEST" )
done

for i in $(getflags env)
do
    SRC="$(printf "%s" "$i" | cut -d" " -f1)"
    DEST="$(printf "%s" "$i" | cut -d" " -f2-)"
    FLAGS+=( --setenv "$SRC" "$DEST" )
done

IFS=$' \t\n'

WD="$(getflags wd)"
if [ "$WD" ]; then
    FLAGS+=( --chdir "$WD" )
fi

CMD="$(getflags cmd)"
if [ "$DEVSHELL" = 1 ]; then
    CMD=bash
fi

REDIR=( )

if [ "$COMPOSE_PASSWD" = 1 ]; then
    PASSWD="$USER:x:$UID:$UID::/tmp/home:/bin/bash"
    REDIR+=( "9<<<$PASSWD" )
    exec 9<<<"$PASSWD"
    FLAGS+=( --bind-data 9 /etc/passwd )
fi

FLAGS+=( "${ENV[@]}" )

FLAGS+=( -- $CMD )

shift 1

printf "\nenv - %s %s %s %s\n\n" "$IMAGE_DIR/static/bwrap" "${REDIR[*]}" "${FLAGS[*]}" "$*"

if [ "$DEVSHELL" = 1 ]; then
    printf "DEVSHELL=1 specified, not running %s\n\n" "$(getflags cmd)"
fi

set +e

trap 'game_stop; exit' EXIT TERM INT
game_start

env - "$IMAGE_DIR/static/bwrap" "${FLAGS[@]}" "$@"

for i in $(getflags clean)
do
    # TODO: Only print this message if there is something to delete
    printf "Cleaning up "
    case "$i" in
        conf)
            find "$IMAGE_DIR/$i/$GAME" -size 0 | tr '\n' ' '
            find "$IMAGE_DIR/$i/$GAME" -size 0 -delete
        ;;
        *)
            printf "\nCannot clean %s\n" "$i"
            continue
        ;;
    esac
    printf "\n"
done
