dlpkg pkg SDL2_net{,-devel}
sed -i 's@prefix=/usr@prefix=/tmp/src/pkg@' pkg/lib/pkgconfig/SDL2_net.pc
sed -i '/Cflags:/s/: /: -I${includedir} /' pkg/lib/pkgconfig/SDL2_net.pc

untar blobwars
cd blobwars

sed -i 's/-Werror//' Makefile

PKG_CONFIG_PATH=/tmp/src/pkg/lib/pkgconfig \
PREFIX=/tmp/bin \
BINDIR=/tmp/bin/ \
makei-st

libcopy /tmp/bin/libs /tmp/src/pkg/lib/*.so* /usr/lib/libSDL2_mixer*.so*

mksq-st
