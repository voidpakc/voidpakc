mkdir -p font
cd font
ar x /tmp/dl/1
tar -xf data.tar*

cd ..
untar chromium
cd chromium

cat "$SCDIR"/patches/*.patch | patch -p1

# SDL2 has some bugs around fullscreen mode
./configure --disable-sdl2 --enable-openal --disable-sdlmixer --disable-dependency-tracking \
            --prefix=/tmp --with-font-path=/tmp/share/gothub__.ttf

makei-st

cp ../font/usr/share/fonts/truetype/uralic/gothub__.ttf /tmp/share
cp -r ../font/usr/share/doc/fonts-uralic /tmp/share

mkdir /tmp/doc
cp AUTHORS COPYING README /tmp/doc
mv /tmp/share/doc/chromium-bsu /tmp/doc
mv /tmp/share/man/man6/chromium-bsu.6 /tmp/doc

mksquashfs /tmp/{bin,share,doc} /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
