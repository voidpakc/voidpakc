dlpkg pkg SDL2_net{,-devel}
sed -i 's@prefix=/usr@prefix=/tmp/src/pkg@' pkg/lib/pkgconfig/SDL2_net.pc
sed -i '/Cflags:/s/: /: -I${includedir} /' pkg/lib/pkgconfig/SDL2_net.pc

untar crispy
cd crispy

patch -p1 <<EOF
--- a/src/d_iwad.c
+++ b/src/d_iwad.c
@@ -22,2 +22,5 @@
 #include <string.h>
+#include <sys/types.h>
+#include <sys/stat.h>
+#include <unistd.h>

@@ -501,6 +504,14 @@
     free(filename);
     if (probe != NULL)
     {
+        struct stat st;
+        stat(probe, &st);
+        if (st.st_size < 2000)
+        {
+            // HACK: This won't be an IWAD file!
+            free(probe);
+            return NULL;
+        }
         return probe;
     }

--- a/src/setup/mainmenu.c
+++ b/src/setup/mainmenu.c
@@ -219,1 +219,2 @@
+        GetLaunchButton(),
         TXT_NewButton2("Configure Display",
@@ -230,2 +231,1 @@
                        (TxtWidgetSignalFunc) CompatibilitySettings, NULL),
-        GetLaunchButton(),
EOF

mkdir build
cd build

PKG_CONFIG_PATH=/tmp/src/pkg/lib/pkgconfig \
cmake-st

make-st

cd ..

libcopy /tmp/bin/libs /tmp/src/pkg/lib/*.so* /usr/lib/libSDL2_mixer*.so*

mksquashfs AUTHORS COPYING.md ChangeLog NEWS.md README.md man \
           build/src/crispy-{doom,heretic,hexen,server,setup,strife} \
           build/src/{midiread,mus2mid} \
           /tmp/bin/libs \
           /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
