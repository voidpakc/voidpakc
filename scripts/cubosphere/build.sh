dlpkg pkg lua51{,-devel} mpg123 vorbis-tools

untar cubosphere
cd cubosphere/data/music

rm ./*.wav ||:

# Transcode all the music to ogg
for i in ./*.mp3
do
    rm "$i.ogg" ||:
    /tmp/src/pkg/bin/mpg123 --long-tag -w "$i.wav" "$i" 2>tag.txt
    FLAGS=( )
    for x in "l album" "a artist" "c comment" "G genre" "t title" "c year"
    do
        w1="$(printf "%s" "$x" | cut -d" " -f1)"
        w2="$(printf "%s" "$x" | cut -d" " -f2)"
        n="$(grep -i "^[[:space:]]$w2:" tag.txt | cut -d: -f2- | sed 's/^ *//')"
        if [ "$n" ]; then
            if [ "$w1" = "c" ]; then
                FLAGS+=( "-c" "$w2=$n" )
            else
                FLAGS+=( "-$w1" "$n" )
            fi
        fi
    done
    ( /tmp/src/pkg/bin/oggenc -Q "${FLAGS[@]}" "$i.wav" -o "$i.ogg"; printf "%s\n" "$i"; rm "$i.wav" ) &
done

wait

rm ./*.mp3 ||:

cd ../../src

patch -p1 -i "$SCDIR"/ogg.patch

CPPFLAGS="$CFLAGS -I/tmp/src/pkg/include/lua5.1/" \
CFLAGS="$CFLAGS -L/tmp/src/pkg/lib" \
make-st DATADIR=/tmp/bin/data

cd ..

cp -a cubosphere data documents /tmp/bin

libcopy /tmp/bin/libs /tmp/src/pkg/lib/liblua*.so* \
                      /usr/lib/lib{GLEW,SDL_mixer}*.so*

mksq-st
