untar darkplaces
cd darkplaces

# disable lto?

# Stop unfixed CFLAGS from "leaking" into the build
OCF="$CFLAGS"
unset CFLAGS
unset LDFLAGS

# > NOTE: *never* *ever* use the -ffast-math or -funsafe-math-optimizations flag
#     -- makefile.inc
CFLAGS="$(printf "%s" "$OCF" | sed 's/-Ofast/-O3/g;s/-ffast-math//g;s/-funsafe-math-optimizations//g')"
# "Normal" optimizations from makefile.inc.
# The "Experimental" ones cause internal compiler errors in GCC 9.2
CFLAGS="-fno-math-errno -ffinite-math-only -fno-rounding-math -fno-signaling-nans -fno-trapping-math $CFLAGS"

make-st CPUOPTIMIZATIONS="$CFLAGS" sdl-release sv-release

mksquashfs darkplaces-sdl darkplaces-dedicated COPYING darkplaces.txt /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
