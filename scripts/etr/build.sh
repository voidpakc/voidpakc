untar 1 sfml
mkdir -p sfml/build
cd sfml/build

cmake-st \
      -DSFML_USE_SYSTEM_DEPS=ON \
      -DSFML_INSTALL_PKGCONFIG_FILES=1

makei-st

cd ../..
untar etr
cd etr

PKG_CONFIG_PATH=/tmp/bin/lib/pkgconfig \
./configure --prefix=/tmp/bin

makei-st

mv /tmp/bin/share/doc/etr /tmp/bin/doc
cp README NEWS AUTHORS COPYING /tmp/bin/doc

mksq-st
