printf "VERSION="
# Fun fact: with xbps-fetch SF returns a different page to
# wget (which has a link to the Windoze binaries).
cat /tmp/update/0 | grep Latest -C3 | grep -o 'etr-[0-9]*\.[^t]*' |
    rev | cut -d. -f2- | rev | cut -d- -f2
