untar hyperrogue
cd hyperrogue

set +v

tput bold
printf "\n\nWarning: Compilation will use 1.5 GB of RAM!\n\n\n"
sleep 5
tput sgr0

set -v

./autogen.sh

CXXFLAGS="`pkg-config sdl --cflags` $CXXFLAGS" ./configure --prefix=/tmp/bin

makei-st

mv /tmp/bin/bin/hyperrogue /tmp/bin/hyperrogue
rmdir /tmp/bin/bin ||:

mv /tmp/bin/share/doc/hyperrogue /tmp/bin/doc
rmdir /tmp/bin/share/doc ||:
cp COPYING /tmp/bin/doc ||:

libcopy /tmp/bin/libs /usr/lib/lib{GLEW,SDL_gfx,SDL_mixer-1.2}.so*

mksq-st
