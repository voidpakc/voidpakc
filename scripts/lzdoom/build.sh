untar gzdoom
cd gzdoom

do-patch

mkdir build
cd build

# Building with fancy CFLAGS causes a number of bugs, e.g.:
#  - LTO causes an internal compiler error
#  - AI can see through walls
#  - Enemies seem to take more shots to kill
#  - After going to the secret area with the armour in the first
#    level of DOOM I, the door back won't open
export CFLAGS="$(printf "%s" "$CFLAGS" | sed 's/-flto[^ ]*//g')"

export CXXFLAGS="$CFLAGS"
export LDFLAGS="$CFLAGS"

cmake-st

make-st

mksquashfs lzdoom *pk3 ../docs ../README.md /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
