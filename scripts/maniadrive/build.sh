# TODO: We don't actually need this for anything, work out a way to
# remove it.
dlpkg pkg v4l-utils-devel

untar maniadrive
cd maniadrive

untar 2 ode
cd ode

patch -p1 -i "$SCDIR"/ode.patch

sed -i 's/.*vsnprintf.*//;s/\(FUNC.*TEST\) 10/\1 9/' configurator.c
sed -i '/C_FLAGS=/s/$/ -fpermissive/' config/makefile.unix-gcc

make-st configure
make-st -j1

cd ..

patch -p1 -i "$SCDIR"/maniadrive.patch

make-st libraydium.a

mkdir -p /tmp/src/pkg/lib
for l in lib{xml2,curl}
do
    ln -s "$(printf "%s\n" /usr/lib/$l.so* | head -n1)" /tmp/src/pkg/lib/$l.so
done

gcc -O3 mania_drive.c -DFORCE_LIBRAYDIUM -Iode/include -I/tmp/src/pkg/include -c -o mania_drive.o

g++ mania_drive.o libraydium.a ode/lib/libode.a -L/tmp/src/pkg/lib \
    -lGL -lGLEW -lGLU -lX11 -lXinerama -lalut -lcurl -ldl -ljpeg -lm \
    -logg -lopenal -lresolv -lvorbis -lvorbisfile -lxml2 -lz \
    -Xlinker -zmuldefs -o mania_drive

untar 1 /tmp/bin

cp mania_drive /tmp/bin/game/

libcopy /tmp/bin/libs /usr/lib/lib{curl,GLEW,nghttp2,ssh2}.so*

mksq-st
