untar hc-media
cd hc-media

mv media/config.xml media/config.xml.template
rm -rv media/userdata
ln -s /tmp/config.xml media/config.xml
ln -s /tmp/userdata media/userdata

mksquashfs . /tmp/out/data.img "${MKSQUASHFS_OPTIONS[@]}"
