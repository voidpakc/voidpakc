untar hcraft

# TODO:
# Investigate problems
#   - Which CFLAG exactly is it?
#   - Is it in Irrlicht or the HCraft source?
#CFLAGS="$(printf "%s" "$CFLAGS" | sed 's/-Ofast/-O3/g;s/-ffast-math//g')"
unset CFLAGS
CFLAGS="-O2"

cd hcraft/libs/irrlicht/source/Irrlicht/
cat Irrlicht-gcc.cbp | grep "<Unit" | grep -v "\.\(h\|txt\)\"" | grep -v MacOS | cut -d'"' -f2 | grep -v "^libpng/\|^zlib/" | sort >/tmp/irrcompile.txt

sed -i 's/int64_t/uint64_t/' aesGladman/sha2.h

# Is this the first one-liner (well, not quite...) build system?

cat /tmp/irrcompile.txt | grep c$ | xargs -t -n1 -P"$MAKEJOBS" \
    gcc $CFLAGS -ffast-math -D_IRR_STATIC_LIB_ -I../../include $(pkg-config libpng --cflags) -fstrict-aliasing -Wno-narrowing -c

cat /tmp/irrcompile.txt | grep cpp$ | xargs -t -n1 -P"$MAKEJOBS" \
    g++ $CFLAGS -ffast-math -D_IRR_STATIC_LIB_ -I../../include $(pkg-config libpng --cflags) -fno-exceptions -fno-rtti -fstrict-aliasing -c

gcc-ar csrD irrlicht.a $(find | grep "\.o$")


cd /tmp/src/hcraft/src
cat hover.cbp | grep "<Unit" | grep -v "\.\(h\|txt\|rc\)\"" | cut -d'"' -f2 | tr '\\' / | sort >/tmp/hccompile.txt

# TODO: Compile the editor as well

cat /tmp/hccompile.txt | xargs -t -n1 -P"$MAKEJOBS" \
    g++ $CFLAGS -fno-strict-aliasing -fno-rtti -fno-exceptions -Df_IRR_STATIC_LIB_ -DNDEBUG -DHOVER_RELEASE -I../libs/irrlicht/include -I/usr/include/freetype2 -c

g++ $CFLAGS -o hcraft $(find | grep "\.o$") ../libs/irrlicht/source/Irrlicht/irrlicht.a -lGL -lGLU -lXext -lX11 -lpthread -lvorbisfile -lvorbisenc -lvorbis -logg -lfreetype -lSDL -lopenal -lalut -lXrandr -lXxf86vm $(pkg-config libpng --libs)

mkdir media

mksquashfs hcraft media ../doc ../README.md /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
