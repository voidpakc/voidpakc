sed -i 's@prefix=/usr@prefix=/tmp/src/pkg@' pkg/lib/pkgconfig/SDL2_net.pc
sed -i '/Cflags:/s/: /: -I${includedir} /' pkg/lib/pkgconfig/SDL2_net.pc

untar tyrian
cd tyrian

PKG_CONFIG_PATH=/tmp/src/pkg/lib/pkgconfig \
make-st CFLAGS="-pedantic -MMD -Wall -Wextra -Wno-missing-field-initializers $CFLAGS" \
        LDFLAGS="-L/tmp/src/pkg/lib"

mkdir install

cp -r opentyrian README NEWS COPYING CREDITS linux/man/opentyrian.6 doc install

cd install

7z x /tmp/dl/data
mv tyrian* data

libcopy libs /tmp/src/pkg/lib/*.so*

mksquashfs . /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
