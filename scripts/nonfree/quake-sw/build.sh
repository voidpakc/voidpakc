tar -xf /tmp/dl/0

echo 'These data files come from shareware Quake, which can be downloaded from, e.g.

https://archive.org/download/msdos_Quake106_shareware/msdos_Quake106_shareware.zip
' >README.txt

mksquashfs id1 README.txt /tmp/out/data.img -keep-as-directory "${MKSQUASHFS_OPTIONS[@]}"
