untar oa
cd oa

do-patch

# LTO causes a segfault on startup (GCC 9.2 / armv7l)
sed -i '/flto/d' Makefile
CFLAGS="$(printf "%s" "$CFLAGS" | sed 's/-flto[^ ]*//g')"

CFLAGS="$CFLAGS -I/tmp/src/pkg/include" \
LDFLAGS="$LDFLAGS -L/tmp/src/pkg/lib" \
make-st

mkdir -p /tmp/bin/{doc,baseoa,missionpack}
cp build/release-*/openarena* /tmp/bin/openarena
cp build/release-*/oa_ded* /tmp/bin/oa_ded

cp COPYING.txt ChangeLog README.md id-readme.txt voip-readme.txt md4-readme.txt /tmp/bin/doc

libcopy /tmp/bin/libs /tmp/src/pkg/lib/*.so*

mksq-st
