dlpkg pkg jsoncpp-devel jsoncpp
sed -i 's@/usr@/tmp/src/pkg@' pkg/lib/pkgconfig/jsoncpp.pc

untar pingus
cd pingus

# TODO: Checkout specific revisions
# Maybe scrape info from GitLab somehow?

awk '
/^\tpath = / {
    path = $3;
    system("mkdir -p -- " path);
}
/^\turl = / {
    remote = $3;
    printf("Remote: %s\n", remote);
    system("git clone -- " remote " " path);
    path = "";
}
' .gitmodules

sed -i '/#include "json/s/^/#include <memory>\n/' src/util/json_writer_impl.cpp

mkdir -p build
cd build

LDFLAGS="$LDFLAGS -L/tmp/src/pkg/lib" \
PKG_CONFIG_PATH=/tmp/src/pkg/lib/pkgconfig \
cmake-st

makei-st

cd ..

mkdir -p /tmp/bin/doc
cp AUTHORS COPYING NEWS README.md /tmp/bin/doc

mkdir -p /tmp/bin/libs
libcopy /tmp/bin/libs /tmp/src/pkg/lib/*.so* /usr/lib/lib{SDL2_mixer,modplug}*.so*

mksq-st
