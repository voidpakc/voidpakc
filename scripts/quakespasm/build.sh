# Use SDL2 instead of SDL1?
# The mouse feels nicer in SDL2, but it still has a few bugs.
SDL2=1

untar quakespasm
cd quakespasm/Quake

if [ "$(printf "%s" "$CFLAGS" | grep -- "-O\(3\|fast\)")" ]; then
    grep -Fvx 'CFLAGS += -O2' Makefile > Makefile.new
    mv Makefile.new Makefile
fi

make-st USE_SDL2="$SDL2" LDFLAGS="$CFLAGS"

mksquashfs quakespasm quakespasm.pak ../LICENSE.txt ../Quakespasm-Music.txt ../Quakespasm.html ../Quakespasm.txt /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
