untar stk

cd stk

cat "$SCDIR"/patches/*.patch | patch -p1

mkdir -p build
cd build

LDFLAGS="$LDFLAGS -L/tmp/src/pkg/lib -Wl,-rpath-link,/tmp/src/pkg/lib" \
cmake-st \
    -DUSE_GLES2=1 \
    -DBUILD_RECORDER=0 \
    -DCHECK_ASSETS=0 \
    -DBLUEZ_INCLUDE_DIR=/tmp/src/pkg/include \
    -DBLUEZ_LIBRARY=/tmp/src/pkg/lib/libbluetooth.so \
    -DHARFBUZZ_INCLUDEDIR=/tmp/src/pkg/include \
    -DHARFBUZZ_LIBRARY=/tmp/src/pkg/lib/libharfbuzz.so \
    -DFRIBIDI_INCLUDE_DIR=/tmp/src/pkg/include \
    -DFRIBIDI_LIBRARY=/tmp/src/pkg/lib/libfribidi.so \
    -DCURL_INCLUDE_DIR=/tmp/src/pkg/include \
    -DCURL_LIBRARY_RELEASE=$(printf "%s\n" /lib/libcurl.so.* | tail -n1) \
    -DOPENSSL_INCLUDE_DIR=/tmp/src/pkg/include \
    -DOPENSSL_CRYPTO_LIBRARY=$(printf "%s\n" /lib/libcrypto.so.* | tail -n1)

makei-st

cp -a ../NETWORKING.md ../CHANGELOG.md ../COPYING ../README.md ../doc /tmp/bin/

rm -r /tmp/bin/share/supertuxkart/data
mkdir /tmp/bin/share/supertuxkart/data

libcopy /tmp/bin/libs \
        /usr/lib/lib{curl,sqlite3,GLEW,nghttp2,ssh2}.so.* \
        /tmp/src/pkg/lib/lib{bluetooth,fribidi,glib,graphite,harfbuzz}*.so*

mksq-st
