dlpkg pkg libcurl-devel
cp -av /usr/lib/libcurl* pkg/lib/

untar supertux
mkdir -p supertux/build
cd supertux/build

# TODO: Install libraqm
cmake-st \
      -DCURL_INCLUDE_DIR=/tmp/src/pkg/include \
      -DCURL_LIBRARY_RELEASE=/tmp/src/pkg/lib/libcurl.so
#      -DENABLE_OPENGLES2=1

makei-st

libcopy /tmp/bin/libs /usr/lib/libboost_{filesystem,locale,system,date_time,chrono,thread}.so.* \
                      /usr/lib/libicu{data,i18n,uc}.so.* \
                      /usr/lib/lib{curl,nghttp2,ssh2,GLEW}.so*

mksq-st
