untar tyrquake
cd tyrquake

mkdir -p build/include
zstdcat "$SCDIR"/tyrquake_icon_128.h.zst >build/include/tyrquake_icon_128.h

sed -i 's/-O2/-O3/' Makefile

PATH="$PATH:/tmp/src/pkg/bin" \
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/tmp/src/pkg/lib" \
    make-st

mksquashfs bin readme.txt readme-id.txt man gnu.txt changelog.txt /tmp/out/bin.img "${MKSQUASHFS_OPTIONS[@]}"
