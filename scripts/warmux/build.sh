cp -av /usr/lib/libxml2.so* pkg/lib/

untar warmux
cd warmux

cp /tmp/dl/guess config.guess
cp /tmp/dl/sub config.sub

CFLAGS="$CFLAGS -I/tmp/src/pkg/include -I/tmp/src/pkg/include/SDL"

# Don't you just *love* broken configure scripts?
sed -i '/Please upgrade SDL_gfx/s/.*/:/' configure

# Who needs diff and patch when you have sed?
sed -i '/SDLNet_Read32(buffer)/{s/^/char tb[4]; memcpy(tb, buffer, 4); /;s/(buffer)/(tb)/}' lib/warmux/action/action.cpp
sed -i 's/Z_BEST_COMPRESSION/9/' src/graphic/surface.cpp
sed -i 's/return false;/return 0;/' src/interface/weapon_menu.cpp
sed -i '/stdio.h/s/$/\n#include <unistd.h>/' tools/list_games/main.cpp
sed -i '/def __SYM/s/if/ifn/;s/namlen/name/' lib/warmux/tools/file_tools.cpp
sed -i '/while (a < 0)/s/.*/if(a<0)a=FIX16_2PI-((-(a+1))%FIX16_2PI+1);/' lib/fixedpoint/fixed_func.cpp

export CFLAGS="$CFLAGS -fsanitize=address"

CFLAGS="$CFLAGS" CXXFLAGS="$CFLAGS" \
ac_cv_lib_SDL_gfx_rotozoomSurfaceXY=yes \
ac_cv_lib_SDL_image_IMG_Load=yes \
ac_cv_lib_SDL_mixer_Mix_OpenAudio=yes \
ac_cv_lib_SDL_ttf_TTF_OpenFont=yes \
ac_cv_lib_SDL_net_SDLNet_Init=yes \
LIBXML2_CFLAGS=-I/tmp/src/pkg/include/libxml2 \
LIBXML2_LIBS="-L/tmp/src/pkg/lib -lxml2" \
./configure --prefix=/tmp/bin

makei-st

mv /tmp/bin/bin/* /tmp/bin
rmdir /tmp/bin/bin ||:

mkdir -p /tmp/bin/doc
cp -r AUTHORS COPYING ChangeLog README doc/howto_play{,_online} /tmp/bin/doc

libcopy /tmp/bin/libs /tmp/src/pkg/lib/libSDL_net-1.2.so* /usr/lib/libSDL_{gfx,mixer-1.2}.so*

mksq-st
