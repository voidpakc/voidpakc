#!/bin/bash

set -e

usage() {
    printf "USAGE: %s GAME\n" "$0"
}

if ! [ -e "$(basename "$0")" ] && ! [ -e bootstrap-void.sh ]; then
    printf "This script must be run in the repo directory\n"
    exit 1
fi

if ! [ "$1" ] || [ "${1:0:1}" = "-" ]; then
    usage
    exit 1
fi

if [ -e config ]; then
    . ./config
else
    printf "Could not find config file\n"
    exit 1
fi

for GAME in "scripts/$1" "$1" "$(dirname "$1")" ""
do
    if [ -e "$GAME"/build.sh ]; then
        break
    fi
done

if ! [ -e "$GAME" ]; then
    printf 'Could not find "%s"\n' "$1"
    exit 1
fi

GNAME="$(basename -- "$GAME")"

printf     "Information for %s:\n\n" "$GNAME"

printf     "Source directory:               %s\n" "$(realpath -- "$GAME")"

BIN="$IMAGE_DIR/bin/$GNAME.img"
if [ -e "$BIN" ]; then
    printf "Binary squashfs image:          %s\n" "$BIN"
fi
BDIR="$IMAGE_DIR/bin/$GNAME"
if [ -d "$BDIR" ]; then
    printf "Binary dir (replaces image):    %s\n" "$BDIR"
elif [ -e "$BIN" ]; then
    printf "To extract:                     unsquashfs -dest %s %s\n" "$BDIR" "$BIN"
else
    printf "No installed binaries.\n"
fi
BMD="$IMAGE_DIR/mnt/bin-$GNAME"
if [ -d "$BMD" ]; then
    printf "Mounted on:                     %s\n" "$BMD"
fi

DATA="$IMAGE_DIR/data/$GNAME.img"
if [ -e "$DATA" ]; then
    printf "Data squashfs image:            %s\n" "$DATA"
fi
DDIR="$IMAGE_DIR/data/$GNAME"
if [ -d "$DDIR" ]; then
    printf "Data dir (replaces image):      %s\n" "$DDIR"
elif [ -e "$DATA" ]; then
    printf "To extract:                     unsquashfs -dest %s %s\n" "$DDIR" "$DATA"
else
    printf "No installed data files.\n"
fi
DMD="$IMAGE_DIR/mnt/data-$GNAME"
if [ -d "$DMD" ]; then
    printf "Mounted on:                     %s\n" "$DMD"
fi

CONF="$IMAGE_DIR/conf/$GNAME"
if [ -d "$CONF" ]; then
    printf "Config files / saves:           %s\n" "$CONF"
fi

for RD in "$GAME"/*.rundat; do
    TNAME="$(basename -- "$RD" .rundat)"
    printf "\n    Target:                         %s\n" "$TNAME"
    printf "    In-tree path:                   %s\n" "$RD"
    IRD="$IMAGE_DIR/run/$TNAME.rundat"
    if ! [ -e "$IRD" ]; then
        printf "    Target not installed.\n"
    elif ! [ "$(sha256sum <"$RD")" = "$(sha256sum <"$IRD")" ]; then
        printf "        Target installed, but in-tree copy differs.\n"
        printf "        Printing information for installed copy.\n"
        printf "    Installed path:                 %s\n" "$IRD"
        RD="$IRD"
    else
        printf "    Installed path:                 %s\n" "$IRD"
    fi
    FEAT="$(grep "^use " -- "$RD" | cut -d" " -f2- | tr '\n' ' ')"
    if [ "$FEAT" ]; then
        printf "    Uses features:                  %s\n" "$FEAT"
    fi
    SUP="$(grep "type game" -- "$RD" | cut -d" " -f3- | tr '\n' ' ')"
    if [ "$SUP" ]; then
        printf "    Compatible data types:          %s\n" "$SUP"
    else
        SUP="$(grep "type data" -- "$RD" | cut -d" " -f3- | tr '\n' ' ')"
        if [ "$SUP" ]; then
            printf "    Needs engines compatible with:   %s\n" "$SUP"
        fi
    fi
    if [ "$SUP" ]; then
        printf "    Compatible:\n"
        OIFS="$IFS"
        IFS=$'\n'
        for t in $(printf "%s" "$SUP" | tr ' ' '\n')
        do
            for x in $(find scripts -name "*.rundat" |
                           xargs grep -l "type data.*$t\( \|$\)" -- |
                           LC_COLLATE=C sort)
            do
                TN="$(dirname -- "$x" | cut -d/ -f2-)"
                if [ -e "$IMAGE_DIR/run/$(basename -- "$x")" ]; then
                    printf "     %s %s (installed)\n" - "$TN"
                else
                    printf " %s %s (not installed)\n" - "$TN"
                fi
            done
        done
        IFS="$OIFS"
    fi
done

