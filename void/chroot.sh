#!/bin/sh
#
# This chroot script uses a plain chroot
#
set -e
readonly MASTERDIR="$1"
readonly DISTDIR="$2"
readonly HOSTDIR="$3"
readonly EXTRA_ARGS="$4"
shift 4

if ! command -v chroot >/dev/null 2>&1; then
	exit 1
fi

if [ -z "$MASTERDIR" -o -z "$DISTDIR" ]; then
	echo "$0 MASTERDIR/DISTDIR not set"
	exit 1
fi

DIR="$(mktemp -d)"

mount -R "$MASTERDIR" "$DIR"
mkdir -p "$DIR"/void-packages "$DIR"/dev "$DIR"/tmp "$DIR"/proc "$DIR"/host
mount -R "$DISTDIR" "$DIR"/void-packages
mount -R /dev "$DIR"/dev
mount -t tmpfs tmpfs "$DIR"/tmp
mount -B /proc "$DIR"/proc
[ "$HOSTDIR" ] && mount -R "$HOSTDIR" "$DIR"/host

chroot "$DIR" $EXTRA_ARGS "$@"
