#!/bin/bash

set -e

. "$CFPATH"config

PYTHON=0

export CFLAGS
export CXXFLAGS="$CFLAGS"
export LDFLAGS="$CFLAGS"

bold() {
    tput bold
}

normal() {
    tput sgr0
}

bprintf() {
    bold
    printf "$@"
    normal
}

bprintf "Compiling %s...\n\n" "$(basename "$(pwd)")"
bprintf "Environment:\n"

( set -o posix; set )

DLNAMES=( )
NDOWNLOAD=( )
NSHA256SUM=( )

[dl() {
: ]
    DLNAMES+=( "${1::-1}" )
    if [ "$URL" ]; then
        NDOWNLOAD+=( "$URL" )
        NSHA256SUM+=( "$SHA256SUM" )
    fi
}

bprintf "\nProgram info:\n"

VSET="$(printf "%s" "$-" | tr -cd v)"
set -v
. info
[ "$VSET" ] || set +v

if [ "$URL" ]; then
    DOWNLOAD=( "${NDOWNLOAD[@]}" "$URL" )
    SHA256SUM=( "${NSHA256SUM[@]}" "$SHA256SUM" )
else
    DLNAMES=( $(seq 0 $((${#DOWNLOAD[@]} - 1))) )
fi

if [ "$NO_UPDATE" = 1 ] || [ "${#UPDATE[@]}" = 0 ]; then
    bprintf "\nSkipping update check.\n"
else
    bprintf "\nChecking for updates...\n"
    rm -r /tmp/update ||:
    mkdir /tmp/update
    set +e
    COUNT="${#UPDATE[@]}"
    for ((i=0; i<"$COUNT"; ++i))
    do
        DL="${UPDATE[$i]}"
        # TODO: Use wget here too
        timeout 10 xbps-fetch -o /tmp/update/"$i" "$DL" ||
            bprintf "Downloading %s timed out.\n" "$DL"
    done
    UPD="$(
tarver() {
    rev | cut -dt -f2- | cut -c2- | cut -d- -f1 | rev
}
. ./update.sh
)"
    set -e
    OIFS="$IFS"
    IFS=$'\n'
    for x in $(printf "%s" "$UPD")
    do
        VAR="$(printf "%s" "$x" | cut -d= -f1)"
        VAL="$(printf "%s" "$x" | cut -d= -f2-)"
        if ! [ "$VAL" ]; then
            continue
        fi
        CVAL="$(eval printf "%s" '$'${VAR})"
        if ! [ "$VAL" = "$CVAL" ]; then
            bprintf "\nThere is a newer version of %s available than %s,\n" "$FULLNAME" "$CVAL"
            bprintf "consider updating the %s= line in the info file to be\n\n%s=%s\n\n" "$VAR" "$VAR" "$VAL"
            bprintf "You may also need to update the sha256sum value.\n\n"
            sleep 10
        else
            bprintf "\n%s=%s is already the latest version.\n\n" "$VAR" "$VAL"
        fi
    done
    IFS="$OIFS"
fi

COUNT="${#DOWNLOAD[@]}"
SUFF=
if [ "$COUNT" -gt 1 ]; then
    SUFF=s
fi
bprintf "\nDownloading %s source file%s...\n\n" "$COUNT" "$SUFF"

rm -r /tmp/dl ||:
mkdir /tmp/dl

MISMATCH=

for ((i=0; i<"$COUNT"; ++i))
do
    DL="${DOWNLOAD[$i]}"
    SHA="${SHA256SUM[$i]}"
    NAME="${DLNAMES[$i]}"
    if ! [ "$SHA" ]; then
        bprintf "\nERROR: Missing SHA256SUM for %s\n\n" "$DL"
        exit 1
    fi
    if ! [ "$NAME" ]; then
        bprintf "\nERROR: No name for %s\n\n" "$DL"
        exit 1
    fi
    OFILE=/tmp/dlf/"$SHA"
    if [ -e "$OFILE" ]; then
        bprintf "%s already downloaded.\n" "$DL"
        if [ -e "$OFILE".info ]; then
            URL="$(tail -n1 "$OFILE".info)"
            if ! [ "$URL" = "$DL" ]; then
                bprintf "\nWarning: This URL:\n    %s\nis marked as having the same content as this one:\n    %s\n\n" "$URL" "$DL"
                bprintf "If this is an error, change the SHA256SUM in the info file\n(currently %s)\n" "$SHA"
                bprintf "to be something different (e.g. delete a few characters)\n"
                sleep 15
            fi
        fi
    else
        OFILE="$OFILE".tmp
        bprintf "Downloading %s\n" "$DL"
        wget --no-check-certificate --progress=dot:mega -O "$OFILE" "$DL" --xattr -c -T 10 --compression=auto --hsts-file=/tmp/dlf/wget-hsts
        printf "%s\n%s\n" "$(pwd)" "$DL" >"$OFILE".info
    fi
    bprintf "Verifying... "
    CSHA="$(sha256sum "$OFILE" | cut -d" " -f1)"
    FILE=/tmp/dlf/"$CSHA"
    if ! [ "$OFILE" = "$FILE" ]; then
        printf "\n"
        mv -v "$OFILE" "$FILE"
        mv -v "$OFILE".info "$FILE".info ||:
    fi
    if [ "$CSHA" = "$SHA" ]; then
        bprintf "done.\n"
    else
        bprintf "SHA256SUM mismatch!\n"
        bprintf "Expected: %s\n" "$SHA"
        bprintf "Got:      %s\n\n" "$CSHA"
        MISMATCH=1
    fi
    ln -s "$FILE" /tmp/dl/"$NAME"
done

if [ "$MISMATCH" ]; then
    exit 1
fi

bprintf "\nDownloaded files:\n"
ls /tmp/dl/* -l

mkdir -p /tmp/{src,bin,data,share}
rm -r /tmp/out ||:

bprintf "\nExecuting build script:\n\n"

. /tmp/scripts/void/cmpscripts.sh

SCDIR="$(pwd)"
cd /tmp/src

set -ev

. "$SCDIR"/build.sh

set +v

cmpscript-print-dir-sizes

bprintf "Compilation complete\n\n"
