# SPDX-License-Identifier: GPL-2.0-or-later OR BSD-2-Clause

cmpscript-print-dir-sizes() {
    printf "\n\nSIZESRC=%s\nSIZEBLD=%s\nSIZEINST=%s\n\n" \
       "$(du -csh $(realpath /tmp/dl/*) | tail -n1 | cut -d$'\t' -f1)" \
       "$(du -csh /tmp/src | tail -n1 | cut -d$'\t' -f1)" \
       "$(du -csh /tmp/out | tail -n1 | cut -d$'\t' -f1)"
}

libcopy() {
    mkdir -p -- "$1"
    cp -drt "$@"
}

mksq-type() {
    if ! [ "$MKSQUASHFS_OPTIONS" ]; then
        MKSQUASHFS_OPTIONS=( -comp lz4 )
    fi
    mksquashfs /tmp/"$1" /tmp/out/"$1".img "${MKSQUASHFS_OPTIONS[@]}"
}

mksq-st() {
    mksq-type bin
}

mksqd-st() {
    mksq-type data
}

cmake-st() {
    cmake .. \
          -DCMAKE_INSTALL_PREFIX=/tmp/bin \
          -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_AR="$(which gcc-ar)" \
          -DCMAKE_RANLIB="$(which gcc-ranlib)" \
          "$@"
}

make-st() {
    make -j"${MAKEJOBS:-4}" "$@"
}

makei-st() {
    make install -j"${MAKEJOBS:-4}" "$@"
}

untar() {
    if [ "$2" ]; then
        SRC="$1"
        DEST="$2"
    else
        SRC=0
        DEST="$1"
    fi
    if printf "%s" "$SRC" | grep -qx "[0-9]*"; then
        SRC=/tmp/dl/"$SRC"
    fi
    mkdir -pv -- "$DEST"
    tar -C "$DEST" --strip-components=1 -xf "$SRC"
}

dlpkg() {
    if [ "$PYTHON" = 1 ]; then
        return 0
    fi
    DEST="$1"
    shift 1
    mkdir -p "$DEST"
    for i
    do
        /tmp/scripts/void/download-package.sh "$i" "$DEST"/"$i".xbps
        tar -C "$DEST" --strip-components=2 -xf "$DEST"/"$i".xbps
    done
}

do-patch() {
    cat "$SCDIR"/patches/*.patch | patch -p1
}
