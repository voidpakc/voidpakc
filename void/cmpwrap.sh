#!/bin/bash

set -e

bold() {
    tput bold
}

normal() {
    tput sgr0
}

bprintf() {
    bold
    printf "$@"
    normal
}

PYTHON=1

export CFLAGS="$(python3 /tmp/scripts/lib/config.py --get Compilation cflags)"
export CXXFLAGS="$CFLAGS"
export LDFLAGS="$CFLAGS"

MAKEJOBS="$(python3 /tmp/scripts/lib/config.py --get Compilation "compile jobs" |
    sed "s/nproc/`nproc`/" |
    awk '
NF == 1 {
    print($1);
}
$2 == "+" {
    print($1 + $3);
}
$2 == "-" {
    print($1 - $3);
}
')"

if ! grep -q "^[0-9]\+$" <<<"$MAKEJOBS"; then
    MAKEJOBS=4
fi

IFS=" " read -r -a MKSQUASHFS_OPTIONS <<<"$(python3 /tmp/scripts/lib/config.py --get Compilation "mksquashfs")"

bprintf "\nCFLAGS=\"%s\"\n" "$CFLAGS"
bprintf "MAKEJOBS=%s\n" "$MAKEJOBS"
bprintf "mksquashfs flags=\"%s\"\n" "${MKSQUASHFS_OPTIONS[*]}"

bprintf "\nDownloaded files:\n"
ls /tmp/dl/* -l

mkdir -p /tmp/{src,bin,data,share}
rm -r /tmp/out ||:

bprintf "\nExecuting build script:\n\n"

. /tmp/scripts/void/cmpscripts.sh

SCDIR="$(pwd)"
cd /tmp/src

set -ev

. "$SCDIR"/build.sh

set +v

cmpscript-print-dir-sizes

bprintf "Compilation complete\n\n"
