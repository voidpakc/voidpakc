#!/bin/bash

if [ -e "$2" ]; then
    if [ -L "$2" ]; then
        rm -- "$2"
    else
        printf "%s already exists!\n" "$2"
        exit 1
    fi
fi

PKG="$1"

getinfo() {
    DAT="$(xbps-query $1 -Rp repository,pkgver,architecture,filename-sha256 "$PKG" | tr '\n' ' ' | awk '{ print $1"/"$2"."$3".xbps"; print $4; }')"

    URL="$(printf "%s" "$DAT" | head -n1)"
    FILE=/tmp/dlf/vpkg/"$(printf "%s" "$URL" | rev | cut -d/ -f1 | rev)"
    SHA256SUM="$(printf "%s" "$DAT" | tail -n1)"

    if ! [ "$SHA256SUM" ]; then
        printf "Could not find package %s!\n" "$PKG"
        exit 1
    fi
}

getinfo

mkdir -p /tmp/dlf/vpkg/

# Retry download if sha256sum check fails or we get a 404
for i in 2 1 0
do
    if [ -e "$FILE" ]; then
        printf "%s already downloaded\n" "$URL"
    else
        wget --progress=dot:mega -T 10 --hsts-file=/tmp/dlf/wget-hsts -O "$FILE" "$URL" |& tee /tmp/dl.log
        if grep -q "ERROR 404: Not Found." /tmp/dl.log; then
            printf "Could not download from %s, retying with current repository data\n" "$URL"
            getinfo -M
            continue
        fi
    fi

    DLSHA="$(sha256sum "$FILE" | cut -d" " -f1)"
    if [ "$SHA256SUM" = "$DLSHA" ]; then
        break
    else
        if [ -s "FILE" ]; then
            printf "SHA256SUM mismatch!\n"
            printf "Expected: %s\n" "$SHA256SUM"
            printf "Got:      %s\n" "$DLSHA"
        else
            printf "File is empty!\n"
        fi
        rm "$FILE"
        if [ "$i" = 0 ]; then
            exit 1
        fi
    fi
done

ln -s "$FILE" "$2"
