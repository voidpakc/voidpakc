#!/bin/bash

# A safer wrapper round sed

VAR=
FLG=
while :; do
    case "$1" in
        -g)
            FLG="${FLG}g"
            shift 1
            ;;
        -v)
            VAR="$2"
            shift 2
            ;;
        *)
            break
            ;;
    esac
done

if [ "$VAR" ]; then
    REP="$VAR='$(printf "%s" "$2" | sed "s/'/'\\\\''/g")'"
else
    REP="$2"
fi

ESCAPED="$(printf "%s" "$REP" | sed 's/\\/\\\\/g;s/&\|@/\\&/g' | tr '\n' '\0' | sed 's/\x0/\\\n/g')"

sed -i "s@$1@$ESCAPED@$FLG" "$3"
