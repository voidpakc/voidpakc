#!/bin/bash

set -e

mkdir -p /tmp/runtime
yes | xbps-install -r /tmp/runtime -c /var/cache/xbps/ -C /usr/share/xbps.d \
                   -S -R /tmp/void-packages-master/hostdir/binpkgs/ \
                   -R /tmp/fakepkg-tmp \
                   base-minimal bash bubblewrap proxychains-ng ncurses \
                   squashfuse wget \
                   SDL2 SDL SDL2_image SDL_image SDL2_ttf SDL_ttf \
                   libopenal freealut \
                   bzip2 libmad libjpeg-turbo fontconfig ftgl gettext \
                   libvorbis libdrm libXdamage libxshmfence libsensors \
                   libffi libedit libxml2 libjack alsa-plugins-jack \
                   eudev-libudev \
                   strace \

EXCLUDE=( -e )
EXCLUDE+=( share/{doc,info,man,clc} )
EXCLUDE+=( lib/xorg/modules )

I18N="$(python3 /scripts/lib/config.py --get Void i18n)"

if [ "$I18N" = 0 ]; then
    EXCLUDE+=( share/{locale,i18n} )
fi

# TODO: Have a script for generating locales after installation
cp /usr/lib/locale/locale-archive /tmp/runtime/usr/lib/locale ||:
MKSQUASHFS_OPTIONS=( $(python3 /scripts/lib/config.py --get Compilation mksquashfs) )

touch /tmp/runtime/etc/resolv.conf

rm /tmp/runtime.img ||:
mksquashfs /tmp/runtime/* /tmp/runtime.img -all-root "${MKSQUASHFS_OPTIONS[@]}" "${EXCLUDE[@]}" db/xbps
ls -sh /tmp/runtime.img

EXCLUDE+=( cache share/boost-build )

PF="/tmp/pseudo"
: >"$PF"

for i in tmp proc dev sys run home
do
    printf "%s d 755 0 0\n" "/$i"
done >>"$PF"

rm /tmp/runtime-devel.img ||:
mksquashfs /{bin,etc,lib,usr,var} /tmp/runtime-devel.img -all-root "${MKSQUASHFS_OPTIONS[@]}" -pf "$PF" "${EXCLUDE[@]}"
ls -sh /tmp/runtime-devel.img
