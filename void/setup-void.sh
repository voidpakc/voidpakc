#!/bin/bash

# Use the Void default
umask 022

set -ve

mkdir -p /var/cache/xbps

# Configure packages if that hasn't happened yet
if ! [ -s /etc/ssl/certs/ca-certificates.crt ]; then
    xbps-reconfigure -f -a
fi

SCRIPTS_DIR="$(dirname "$(realpath "$0")")/.."

cd /tmp

MUSL="$(python3 "$SCRIPTS_DIR"/lib/config.py --get Void musl)"

LOCALES="$(python3 "$SCRIPTS_DIR"/lib/config.py --get Void locales)"

if ! [ "$MUSL" = 1 ]; then
    if ! [ -e /etc/default/libc-locales.def ]; then
        mv /etc/default/libc-locales{,.def}
    fi

    for i in $(printf "%s" "$LOCALES")
    do
        grep "^#$i " /etc/default/libc-locales.def | cut -d"#" -f2-
    done >/etc/default/libc-locales.new

    if ! [ "$(sha256sum </etc/default/libc-locales)" = "$(sha256sum </etc/default/libc-locales.new)" ]; then
        mv /etc/default/libc-locales.new /etc/default/libc-locales
        xbps-reconfigure -f glibc-locales
    fi
fi

# Conflicts with Mesa, and we don't want it anyway
printf "ignorepkg=rpi-userland\nignorepkg=rpi-userland-devel\n" >/etc/xbps.d/ignore-rpi.conf

rm -r fakepkg-tmp ||:
mkdir -p fakepkg-tmp/pkg
cd fakepkg-tmp

# Create fake packages that pretend to provide shared libs to keep xbps happy
#  libOSMesa - requires libllvm, a ~70MB library
#  libLLVM-8 - required by musl libGL
#  libLLVM-11 - required for CL stuff ?
# TODO: Use ignorepkg for all the stuff
xbps-create -A noarch -n libOSMesa-200.0.0_1 -s "OSMesa" --shlib-provides 'libOSMesa.so.8' pkg
xbps-create -A noarch -n libbcm-8.0.0_7 -s "libbcm" --shlib-provides 'libbcm_host.so' pkg

LLVM_VER="$(xbps-query -R -p run_depends llvm | cut -d'>' -f1 | sed 's/^llvm//')"

xbps-create -A noarch -n libllvm"$LLVM_VER"-"$LLVM_VER".9999.0_1 -s "LLVM" --shlib-provides "libLLVM-$LLVM_VER.so" pkg
xbps-create -A noarch -n libclang-cpp-"$LLVM_VER".9999.0_1 -s "LLVM" --shlib-provides "libclang-cpp.so.$LLVM_VER" pkg

BOOST_VER="$(xbps-query -R -p pkgver boost | cut -d- -f2)"
BOOST_MAJOR="$(printf "%s\n" "$BOOST_VER" | cut -d. -f-2)"

# TODO: Update as Boost updates
xbps-create -A noarch -n boost-python"$BOOST_MAJOR"-"$BOOST_VER" -s "Boost python" pkg

xbps-rindex -a ./*.xbps
xbps-install -y -R . libOSMesa libbcm boost-python"$BOOST_MAJOR" libllvm"$LLVM_VER" libclang-cpp

cd ..

# Essential build packages
xbps-install -y cmake gcc git make pkg-config wget xz zstd p7zip libsanitizer
rm -v /usr/lib/gcc/*/*/gnat1 ||:

# Useful packages
xbps-install -y ncdu strace gdb

if ! gzip -t void-packages.tar.gz; then
    xbps-fetch https://github.com/void-linux/void-packages/archive/master.tar.gz -o void-packages.tar.gz
fi
tar -xf void-packages.tar.gz
cd void-packages-master/

cp "$SCRIPTS_DIR"/void/chroot.sh common/chroot-style/chroot.sh

rm etc/conf ||:

printf "XBPS_CHROOT_CMD='%s'\n" "$CHROOT_MODE" >> etc/conf

if grep -q arm <<<"$(uname -m)"; then
    ARM_NEON="$(python3 "$SCRIPTS_DIR"/lib/config.py --get Void "neon simd")"
fi

# TODO: Fix xbps-src + ./configure + LTO so the below lines can be uncommented
#printf 'XBPS_CFLAGS='\''%s'\''\nXBPS_CXXFLAGS="${XBPS_CFLAGS}"\n' "$CFLAGS" >> etc/conf
#printf "XBPS_LDFLAGS='%s'\n" "$LDFLAGS" >> etc/conf

if [ "$ARM_NEON" = 1 ]; then
    sed -i 's/-mfpu=[^ ]*/-mfpu=neon/g' common/build-profiles/*
fi

MAKEJOBS="$(python3 "$SCRIPTS_DIR"/lib/config.py --get Compilation "compile jobs" |
    sed "s/nproc/`nproc`/" |
    awk '
NF == 1 {
    print($1);
}
$2 == "+" {
    print($1 + $3);
}
$2 == "-" {
    print($1 - $3);
}
')"
printf "XBPS_MAKEJOBS='%s'\n" "$MAKEJOBS" >> etc/conf

./xbps-src binary-bootstrap

cp /usr/share/xbps.d/00-repository-main.conf etc/repos-remote.conf

depfix() {
    for i in $(. srcpkgs/"$1"/template; printf "%s %s %s\n" "$depends" "$hostmakedepends" "$makedepends")
    do
        SRCVER="$(. srcpkgs/"$i"/template; printf "%s\n" "$version")"
        SRCREV="$(. srcpkgs/"$i"/template; printf "%s\n" "$revision")"
        REMOTEVER="$(xbps-query -r masterdir/ -R "$i" -p pkgver | rev | cut -d- -f1)"
        if ! [ "$REMOTEVER" ]; then
            # Use host repo as fallback
            REMOTEVER="$(xbps-query -R "$i" -p pkgver | rev | cut -d- -f1)"
        fi
        REMOTEREV="$(printf "%s" "$REMOTEVER" | cut -s -d_ -f1 | rev)"
        REMOTEVER="$(printf "%s" "$REMOTEVER" | cut -d_ -f2- | rev)"
        if ! [ "$SRCVER" = "$REMOTEVER" ]; then
            printf "Note: %s: Remote (%s) and local (%s) versions do not match.\n" "$i" "$REMOTEVER" "$SRCVER"
            "$SCRIPTS_DIR"/void/safe-replace.sh "^version=.*" "version=$REMOTEVER" srcpkgs/"$i"/template
        fi
        if ! [ "$SRCREV" = "$REMOTEREV" ]; then
            printf "Note: %s: Remote (%s) and local (%s) revisions do not match.\n" "$i" "$REMOTEREV" "$SRCREV"
            "$SCRIPTS_DIR"/void/safe-replace.sh "^revision=.*" "revision=$REMOTEREV" srcpkgs/"$i"/template
        fi
    done
}

if ! [ -e /var/db/xbps/.squashfs-tools-files.plist ]; then
    sed -i '/^makedepends=/s/"$/ libzstd-devel"/;/makejobs/s/$/ ZSTD_SUPPORT=1/' srcpkgs/squashfs-tools/template
    # Sourceforge is unreliable, download from GH
    if ! grep -Fxq version=4.4 srcpkgs/squashfs-tools/template; then
        printf "The squashfs-tools version has been updated.\nYou will need to update the checksum.\n"
    fi
    sed -i '/^wrksrc=/s@=.*@="squashfs-tools-${version}"@' srcpkgs/squashfs-tools/template
    sed -i '/^distfiles=/s@=.*@=https://github.com/plougher/squashfs-tools/archive/${version}.tar.gz@' srcpkgs/squashfs-tools/template
    sed -i '/^checksum=/s@=.*@=a7fa4845e9908523c38d4acf92f8a41fdfcd19def41bd5090d7ad767a6dc75c3@' srcpkgs/squashfs-tools/template
    depfix squashfs-tools
    ./xbps-src pkg squashfs-tools
    xbps-install -y -R hostdir/binpkgs squashfs-tools
fi

SQFUSETXZ="$SCRIPTS_DIR"/void/squashfuse/files/squashfuse-0.1.103.tar.xz
if ! [ -e /var/db/xbps/.squashfuse-files.plist ]; then
    SH256="$(sha256sum "$SQFUSETXZ" | cut -d" " -f1)"
    mkdir -p hostdir/sources/by_sha256
    cp "$SQFUSETXZ" hostdir/sources/by_sha256/"$SH256"_squashfuse-0.1.103.tar.xz
    cp -rn "$SCRIPTS_DIR"/void/squashfuse srcpkgs/
    ln -s squashfuse srcpkgs/squashfuse-devel
    depfix squashfuse
    ./xbps-src pkg squashfuse
    xbps-install -y -R hostdir/binpkgs squashfuse
fi

# To avoid including ICU stuff in the runtime...
# (libxml2 is required by LLVM, which Mesa links
#  against by default if installed)
if ! [ -e /var/db/xbps/.libxml2-files.plist ]; then
    sed -i 's/--with-icu//' srcpkgs/libxml2/template
    depfix libxml2
    ./xbps-src pkg libxml2
    xbps-install -y -R hostdir/binpkgs libxml2
fi

# Essential libraries
if [ "$ARM_NEON" = 1 ] && ! [ -e /var/db/xbps/.libpng-devel-files.plist ]; then
    depfix libpng
    sed -i 's/arm-neon=no/arm-neon=yes/' srcpkgs/libpng/template
    ./xbps-src pkg libpng
    xbps-install -y -R hostdir/binpkgs libpng-devel
fi

if ! [ -e /var/db/xbps/.SDL2-devel-files.plist ]; then
    sed -i 's/arm/nomatch/' srcpkgs/SDL2/template
    if [ "$ARM_NEON" = 1 ]; then
        sed -i 's/--enable-alsa/& --enable-arm-neon/' srcpkgs/SDL2/template
    fi
    depfix SDL2
    ./xbps-src -o '~vulkan' pkg SDL2
    xbps-install -y -R hostdir/binpkgs SDL2-devel
fi

if ! [ -e /var/db/xbps/.SDL-devel-files.plist ]; then
    depfix SDL
    ./xbps-src -o opengl,pulseaudio pkg SDL
    xbps-install -y -R hostdir/binpkgs SDL-devel
fi

xbps-install -y SDL{,2}_{image,ttf,mixer,gfx}-devel

# Very common libraries
xbps-install -y zlib-devel libopenal-devel freealut-devel jack-devel alsa-plugins-jack boost-devel
rm -v /usr/share/icu/*/icudt*l.dat ||:

# Common libraries
#  libmad is an MP3 decoder
#  ftgl is an OpenGL font renderer
xbps-install -y bzip2-devel libjpeg-turbo-devel libmad-devel fontconfig-devel ftgl-devel gettext
xbps-install -y glew-devel

# Deps for compiling Mesa
xbps-install -y meson python3-Mako bison flex wayland-devel wayland-protocols libsensors

# Statically compile some programs:
xbps-install -y patch libcap-devel autoconf automake file
xbps-install -y zlib-devel lzo-devel liblzma-devel liblz4-devel libzstd-devel

if ! [ -e /var/db/xbps/.fuse3-devel-files.plist ]; then
    depfix fuse3-devel
    sed -i '/^configure_args=/s/"$/ --default-library static"/' srcpkgs/fuse3-devel/template
    sed -i 's/\.so/.a/g' srcpkgs/fuse3-devel/template
    ./xbps-src pkg fuse3-devel
    xbps-install -y -R hostdir/binpkgs fuse3-devel
fi

mkdir -p /tmp/static
# Squashfuse
cd /tmp
if ! [ -e /tmp/static/squashfuse ]; then
    mkdir -p squashfuse-static
    cd squashfuse-static
    rm -r ./* ||:
    tar -xf "$SQFUSETXZ"
    cd ./*
    pkgconfig_fuse_CFLAGS="$(pkg-config --cflags fuse3)" \
        pkgconfig_fuse_LIBS="$(pkg-config --libs fuse3) -ldl" ./configure
    # Why does libtool have to make finding the right options so hard?
    AM_LDFLAGS='-all-static' make --trace
    install squashfuse{,_ll,_ls,_extract} /tmp/static
fi

# fuse-overlayfs
cd /tmp
if ! [ -e /tmp/static/fuse-overlayfs ]; then
    mkdir -p fuse-overlayfs
    cd fuse-overlayfs
    rm -r ./* ||:
    FOVERFS_VOID=/tmp/void-packages-master/srcpkgs/fuse-overlayfs
    xbps-fetch "$(. "$FOVERFS_VOID"/template; printf "%s\n" "$distfiles")" -o /tmp/fuse-overlayfs.tgz
    tar xvf /tmp/fuse-overlayfs.tgz
    cd ./*
    ./autogen.sh
    LIBS="-ldl" LDFLAGS="-static" ./configure
    make
    install fuse-overlayfs /tmp/static
fi

# Bubblewrap
cd /tmp
if ! [ -e /tmp/static/bwrap ]; then
    mkdir -p bwrap-static
    cd bwrap-static
    rm -r ./* ||:
    BWRAP_VOID=/tmp/void-packages-master/srcpkgs/bubblewrap
    xbps-fetch "$(. "$BWRAP_VOID"/template; printf "%s\n" "$distfiles")" -o /tmp/bubblewrap.tgz
    tar -xf /tmp/bubblewrap.tgz
    cd ./*
    cat "$BWRAP_VOID"/patches/* | patch -p0
    autoreconf -fi
    CFLAGS='-static' ./configure
    make
    install bwrap /tmp/static
fi
